# Samply Store

Samply Store is a *generic* data access layer library, that provides access to
a PostgreSQL 9.3 database. It uses the JSON datatype feature introduced in
PostgreSQL 9.2 and is therefore incompatible with other relational databases.
Ir requires PostgreSQL >=9.3 because it uses the JSON accessor functions
introduced in 9.3.

# Features

- build your own database scheme using XML
- one class to access all tables: `Resource` (exception: transactions)
- generic properties: add more properties to a `Resource` using `addProperty`,
  no scheme changes necessary. The library uses PostgreSQLs `JSON` datatype
  feature to store your generic properties.
- all tables are versioned: access all changes of one specific `Resource`.
- generic interface for permission management: implement your own `AccessController`
  to meet your requirements.


# Introduction

In contrast to the DAO pattern, this library uses only one class for every
table in the database: `Resource`. A `Resource` is comparable to one row in a table.

An example:

```
Table user

-------------------------------------------------------
|   id        |  name             |  data             |
-------------------------------------------------------
|   3         |  Mark             | {"age":34}        |
|   4         |  Stefan           | {"age":14}        |
-------------------------------------------------------
```

If you use Samply Store to get the user with the ID 3, the library returns a
`Resource` that has two properties: `name` and `age`. The value of the `name`
property is a `StringLiteral` whose String representation is `Mark`. The value
of the `age` property is a `NumberLiteral` whose String representation is `4`.


# Build

In order to build this project, you need to configure maven properly.  See
[Samply.Maven](https://bitbucket.org/medinfo_mainz/samply.maven) for more
information.

Use maven to build the `jar` file:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>store</artifactId>
    <version>${version}</version>
</dependency>
```
