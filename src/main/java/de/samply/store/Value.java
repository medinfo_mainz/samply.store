/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * A value used in resources.
 *
 */
public abstract class Value implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Returns the string representation of this value.
     * @return
     */
    public abstract String getValue();

    /**
     * Tries to convert the value to an integer.
     *
     * @return The converted value or 0 if conversion is not possible.
     */
    public int asInteger() {
        if(this instanceof NumberLiteral) {
            return ((NumberLiteral)this).get().intValue();
        } else {
            try {
                return Integer.parseInt(getValue());
            } catch(NumberFormatException e) {
                return 0;
            }
        }
    }

    /**
     * Tries to convert the value to a float.
     * @return The converted value or 0 if conversion is not possible.
     */
    public float asFloat() {
        if(this instanceof NumberLiteral) {
            return ((NumberLiteral)this).get().floatValue();
        } else {
            try {
                return Float.parseFloat(getValue());
            } catch(NumberFormatException e) {
                return 0;
            }
        }
    }

    /**
     * Tries to convert the value to a long.
     * @return The converted value or 0 if conversion is not possible.
     */
    public long asLong() {
        if(this instanceof NumberLiteral) {
            return ((NumberLiteral)this).get().longValue();
        } else {
            try {
                return Long.parseLong(getValue());
            } catch(NumberFormatException e) {
                return 0;
            }
        }
    }

    /**
     * Tries to convert the value to a number.
     * @return The converted value or 0 if conversion is not possible.
     */
    public Number asNumber() {
        if(this instanceof NumberLiteral) {
            return ((NumberLiteral) this).get();
        } else {
            try {
                return NumberFormat.getInstance().parse(getValue());
            } catch(ParseException e) {
                return 0;
            }
        }
    }

    /**
     * Gets this value as a Resource.
     * @return {@code (Resource) this} if {@code this instanceof Resource}, null otherwise.
     */
    public Resource asResource() {
        if(this instanceof Resource) {
            return (Resource) this;
        } else {
            return null;
        }
    }

    /**
     * Gets this value as a JSONResource.
     * otherwise null.
     * @return {@code (JSONResource) this} if {@code this instanceof JSONResource}, null otherwise.
     */
    public JSONResource asJSONResource() {
        if(this instanceof JSONResource) {
            return (JSONResource) this;
        } else {
            return null;
        }
    }

    /**
     * Gets this value as a TimestampLiteral.
     * otherwise null.
     * @return {@code (TimestampLiteral) this} if {@code this instanceof TimestampLiteral}, null otherwise.
     */
    public Timestamp asTimestamp() {
        if(this instanceof TimestampLiteral) {
            return ((TimestampLiteral)this).value;
        } else {
            return null;
        }
    }

    /**
     * Gets this value as a BooleanLiteral.
     * otherwise null.
     * @return {@code (BooleanLiteral) this} if {@code this instanceof BooleanLiteral}, null otherwise.
     */
    public Boolean asBoolean() {
        if(this instanceof BooleanLiteral) {
            return ((BooleanLiteral)this).value;
        } else {
            return null;
        }
    }

    /**
     * Gets this value as a ResourceIdentifierLiteral.
     * otherwise null.
     * @return {@code (ResourceIdentifierLiteral) this} if {@code this instanceof ResourceIdentifierLiteral}, null otherwise.
     */
    public ResourceIdentifierLiteral asIdentifier() {
        if(this instanceof ResourceIdentifierLiteral) {
            return (ResourceIdentifierLiteral) this;
        } else {
            return null;
        }
    }

    @Override
    public abstract boolean equals(Object o);
}
