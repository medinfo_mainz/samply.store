/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Some Security Utils. Those should be used to hash passwords, generate new random strings,
 * and other things.
 *
 *
 */
public class PasswordHash {

    /**
     * Generates a new random string (32 characters long)
     *
     * @return
     */
    public static String generateSalt() {
        return generateRandomString(32, false);
    }

    /**
     * Generates a new random ASCII string with the given length. If onlyUpperCase is true,
     * only upper case letters will be used (and numbers): [0-9A-Z], otherwise lower and
     * upper case letter will be used [0-9A-Za-z].
     *
     * @param length
     * @param onlyUpperCase
     * @return
     */
    public static String generateRandomString(int length, boolean onlyUpperCase) {
        String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        if (onlyUpperCase)
            alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        int n = alphabet.length();
        String result = new String();
        SecureRandom r = new SecureRandom();

        for (int i = 0; i < length; i++)

            result += alphabet.charAt(r.nextInt(n));

        return result;
    }

    /**
     * Hashes the given clear text password with the given salt and the SHA-512
     * algorithm.
     *
     * @param clearTextPassword
     * @param salt
     * @return
     */
    public static String hashPassword(String clearTextPassword, String salt) {
        return hashPassword(clearTextPassword, salt, "SHA-512", 1, "");
    }

    /**
     * Hashes the given clear text password with the given salt and algorithm.
     *
     * @param clearTextPassword
     * @param salt
     * @param algorithm
     * @return
     */
    public static String hashPassword(String clearTextPassword, String salt, String algorithm) {
        return hashPassword(clearTextPassword, salt, algorithm, 1, "");
    }

    /**
     * Hashes the given clear text password with the given salt and algorithm. Hashes the password
     * "rounds" times.
     *
     * @param clearTextPassword
     * @param salt
     * @param algorithm
     * @param rounds
     * @return
     */
    public static String hashPassword(String clearTextPassword, String salt, String algorithm,
            int rounds) {
        return hashPassword(clearTextPassword, salt, algorithm, rounds, "");
    }

    /**
     * Hashes the given clear text password with the given salt and algorithm. Hashes the password
     * "rounds" times and uses the "postfix" argument for every hash as postfix.
     *
     * @param clearTextPassword
     * @param salt
     * @param algorithm
     * @param rounds
     * @param postfix
     * @return
     */
    public static String hashPassword(String clearTextPassword, String salt,
            String algorithm, int rounds, String postfix) {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            if (rounds <= 0) {
                rounds = 1;
            }
            String toHash = clearTextPassword;

            for (int i = 0; i < rounds; ++i) {
                toHash = hashString(md, toHash + salt + postfix);
            }
            return toHash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Hashes the given input with the given message digest and returns the hex representation.
     * @param md
     * @param input
     * @return
     */
    private static String hashString(MessageDigest md, String input) {
        return convertToHex(md.digest(input.getBytes()));
    }

    /**
     * Converts the given byte array into a hex string representation.
     * @param raw
     * @return
     */
    private static String convertToHex(byte[] raw) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < raw.length; i++) {
            sb.append(Integer.toString((raw[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
