/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.store.TableDefinition.Join;
import de.samply.store.TableDefinition.TableColumn;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * A resource is the samply store representation of a JSON object. Either a JSON resource or a real resource that
 * can be saved in a table in the database.
 *
 */
public abstract class AbstractResource extends Value {

    private static Logger logger = LogManager.getLogger(AbstractResource.class);

    /**
     *
     */
    private static final long serialVersionUID = -6465529019630940439L;

    private HashMap<String, ArrayList<Value>> properties;

    HashMap<String, ArrayList<Value>> get() {
        return properties;
    }

    /**
     * Resets all properties, but leaves everything else intact.
     */
    void reset() {
        properties.clear();
    }

    protected AbstractResource() {
        this.properties = new HashMap<>();
    }

    /**
     * Returns the first value for the given property. If multiple values are available,
     * this method should not be used.
     * @param property
     * @return The requested value or null if the given property does not exist.
     */
    public Value getProperty(String property) {
        if(properties.containsKey(property)) {
            return properties.get(property).get(0);
        } else {
            return null;
        }
    }

    /**
     * Returns all resources for the given property. Only accessible (by the user)
     * resources are returned. This method is different than the getResources method without
     * the model parameter in that way, that this method should be used for properties to
     * <u>other</u> resources only. This method returns only those resources which the user
     * is allowed to access.
     * @param property
     * @return
     */
    public ArrayList<Resource> getResources(String property,
            DatabaseModel<?> model) {
        if (this instanceof Resource) {
            Resource r = (Resource) this;
            TableDefinition tdef = DatabaseDefinitions.get(r.getType());
            TableColumn column = tdef.getColumnForProperty(property);
            String type = null;

            if (column == null) {
                for (Join j : tdef.getJoins()) {
                    if (j.property.equals(property)) {
                        type = j.resourceType;
                    }
                }
            } else {
                type = column.resourceType;
            }

            if (type == null) {
                return new ArrayList<Resource>();
            } else {
                ResourceQuery query = new ResourceQuery(type);
                query.add(Criteria.Equal(r.getType(), BasicDB.ID, r.getId()));
                try {
                    return model.getResources(query);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    return new ArrayList<Resource>();
                }
            }

        } else {
            return new ArrayList<Resource>();
        }
    }

    /**
     * Returns all values for the given property.
     * @param property
     * @return
     */
    public ArrayList<Value> getProperties(String property) {
        if(properties.get(property) == null) {
            return new ArrayList<Value>();
        } else {
            ArrayList<Value> props = properties.get(property);
            if(props.size() > 0 && props.get(0) instanceof ResourceIdentifierLiteral) {
                logger.warn("Accessing ResourceIdentifierLiterals through getProperties!");
                StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
                StackTraceElement e = stackTrace[2];
                logger.warn("Called from " + e.getClassName() + "." + e.getMethodName());
            }

            return properties.get(property);
        }
    }

    /**
     * Adds a Value. Value must not be null.
     * @param property
     * @param value
     */
    public void addProperty(String property, Value value) {
        if(properties.containsKey(property)) {
            properties.get(property).add(value);
        } else {
            setProperty(property, value);
        }
    }

    /**
     * Sets a Value. All existing values for the given property
     * will be deleted. Value must not be null.
     * @param property
     * @param value
     */
    public void setProperty(String property, Value value) {
        ArrayList<Value> values = new ArrayList<>();
        values.add(value);
        properties.put(property, values);
    }

    /**
     * Returns all defined properties for this resource. The returned set may be empty, if this resource
     * has no properties at all.
     * @return
     */
    public Set<String> getDefinedProperties() {
        return properties.keySet();
    }

    /**
     * Removes all property values from the given property. Property must not be null.
     * @param property
     */
    public void removeProperties(String property) {
        properties.remove(property);
    }

    /**
     * Removes a single property value from the given property. Value must not be null.
     * If the specified value is not a value in this resource, it is ignored.
     * @param property
     * @param value
     */
    public void removeProperty(String property, Value value) {
        if(properties.containsKey(property)) {
            for(Value v : properties.get(property)) {
                if(v.equals(value)) {
                    properties.get(property).remove(v);
                }
            }
        }
    }

    /**
     * Adds a non-null StringLiteral value
     * @param property
     * @param value
     */
    public void addProperty(String property, String value) {
        addProperty(property, new StringLiteral(value));
    }

    /**
     * Adds a non-null NumberLiteral value
     * @param property
     * @param value
     */
    public void addProperty(String property, Number value) {
        addProperty(property, new NumberLiteral(value));
    }

    /**
     * Adds a non-null BooleanLiteral value
     * @param property
     * @param value
     */
    public void addProperty(String property, Boolean value) {
        addProperty(property, new BooleanLiteral(value));
    }

    /**
     * Sets a non-null StringLiteral value. All existing values for the given property
     * will be deleted.
     * @param property
     * @param value
     */
    public void setProperty(String property, String value) {
        setProperty(property, new StringLiteral(value));
    }

    /**
     * Sets a non-null NumberLiteral value. All existing values for the given property
     * will be deleted.
     * @param property
     * @param value
     */
    public void setProperty(String property, Number value) {
        setProperty(property, new NumberLiteral(value));
    }

    /**
     * Sets a non-null BooleanLiteral value. All existing values for the given property
     * will be deleted.
     * @param property
     * @param value
     */
    public void setProperty(String property, Boolean value) {
        setProperty(property, new BooleanLiteral(value));
    }

    /**
     * Returns this resouce as JSON string
     * @return
     */
    public String toJson() {
        return ResourceUtils.createJsonElement(this).toString();
    }

}
