/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

/**
 * A resource that is stored in a SQL table.
 *
 */
public class Resource extends AbstractResource implements Identifiable {
    /**
     *
     */
    private static final long serialVersionUID = -558654826077198061L;

    /**
     * The resource type
     */
    private final String type;

    /**
     * The id
     */
    private int id;

    /**
     * Construct an instance with the given type.
     * @param type
     */
    Resource(String type) {
        this.type = type;
        this.id = 0;
    }

    /**
     * Construct an instance with the given type and id.
     * @param type
     * @param id
     */
    Resource(String type, int id) {
        this.type = type;
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getValue() + "::tableResource";
    }

    @Override
    public String getValue() {
        return type + ":" + id;
    }

    @Override
    public String getType() {
        return type;
    }

    /**
     * Returns the resource identifier to identify this resource.
     * @return
     */
    public ResourceIdentifierLiteral getIdentifier() {
        return new ResourceIdentifierLiteral(type, id);
    }

    /**
     * Returns this.id - other.id.
     * @param other
     * @return
     */
    public int compareTo(Resource other) {
        return id - other.id;
    }

    @Override
    public boolean equals(Object o) {
        // TODO: implement this. DO NOT THROW AN EXCEPTION IN A METHOD, THAT MAY BE CALLED BY
        // JRE CLASSES LIKE MAPS, LISTS, ETC.!
        return false;
    }
}
