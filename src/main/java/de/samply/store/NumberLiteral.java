/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.math.BigDecimal;

import de.samply.store.sql.SQLColumnType;

/**
 * A storable NumberLiteral, can store ints, longs, floats and doubles.
 *
 */
public class NumberLiteral extends Literal<Number> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance from a given value.
     * @param value The number that should be represented by this instance.
     */
    public NumberLiteral(Number value) {
        super(value);
    }

    @Override
    public String getValue() {
        return "" + value;
    }

    @Override
    public String toString() {
        return value + "::number";
    }

    @Override
    public boolean equals(Object o) {
        // Because Numbers aren't really comparable, we just
        // use BigDecimal and compare those
        if(o instanceof NumberLiteral) {
            return new BigDecimal(this.value.toString()).compareTo(new BigDecimal(((NumberLiteral) o).get().toString())) == 0;
        } else {
            return false;
        }
    }

    @Override
    public SQLColumnType getCorrespondingSQLType() {
        if(get() instanceof Integer) {
            return SQLColumnType.JSON_INT;
        }

        if(get() instanceof Float) {
            return SQLColumnType.JSON_FLOAT;
        }

        if(get() instanceof Long) {
            return SQLColumnType.JSON_LONG;
        }

        if(get() instanceof Double) {
            return SQLColumnType.JSON_FLOAT;
        }
        return SQLColumnType.JSON_INT;
    }

}
