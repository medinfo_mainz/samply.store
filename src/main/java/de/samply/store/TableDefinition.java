/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.util.ArrayList;

import de.samply.store.sql.SQLColumnType;

/**
 * A definition structure for SQL-Tables. Contains all columns that are available in the
 * actual SQL table as well as all joins necessary to create the SQL SELECT query.
 *
 */
public class TableDefinition {

    /**
     * The real table name in the SQL database.
     */
    public final String table;

    /**
     * The resource type for this SQL table.
     */
    public final String resourceType;

    /**
     * The SQL columns available in the SQL table.
     */
    private ArrayList<TableColumn> columns = new ArrayList<TableColumn>();

    /**
     * The joins that should be executed for SELECT statements.
     */
    private ArrayList<Join> joins = new ArrayList<>();

    /**
     * Creates an instance with the given table name and resource type.
     * @param table
     * @param resourceType
     */
    public TableDefinition(String table, String resourceType) {
        this.table = table;
        this.resourceType = resourceType;
    }

    /**
     * Gets the name of the table represented by this instance.
     */
    public String getTable() {
        return table;
    }

    /**
     * Gets the columns of this table.
     * @return
     */
    public ArrayList<TableColumn> getColumns() {
        return columns;
    }

    /**
     * Returns the joins that should be executed for SELECT statements.
     * @return
     */
    public ArrayList<Join> getJoins() {
        return joins;
    }

    /**
     * Adds a join to this table definition with the specified attributes
     * @param table the table name that must be joined
     * @param resourceType the resource type for this joined table
     * @param joinedColumn the sql colum that must be used for the join
     * @param property the property for the resource
     * @param inverseProperty the inverse property
     */
    public void createJoin(String table, String resourceType, String joinedColumn,
            String property, String inverseProperty) {
        joins.add(new Join(table, resourceType, joinedColumn, property, inverseProperty, null, Join.JoinType.N_TO_1_JOIN));
    }

    public void createJoin(String table, String resourceType, String joinedColumn,
            String property, String inverseProperty, String selectColumn) {
        joins.add(new Join(table, resourceType, joinedColumn, property, inverseProperty, selectColumn, Join.JoinType.N_TO_M_JOIN));
    }

    /**
     * Creates a new SQL column in this table definition with the specified attributes.
     * @param column
     * @param type
     * @param property
     * @param isMandatory
     */
    public void createField(String column, SQLColumnType type, String property, boolean isMandatory) {
        columns.add(new TableColumn(column, type, property, isMandatory));
    }

    /**
     * Creates a new SQL columns in this table definition with the specified attributes.
     * @param column
     * @param type
     * @param property
     * @param resourceType
     * @param isMandatory
     */
    public void createField(String column, SQLColumnType type, String property,
            String resourceType, boolean isMandatory) {
        columns.add(new TableColumn(column, type, property, resourceType, isMandatory));
    }

    /**
     * Returns the SQL columns for the specified property. Null if there is no columns for this property.
     * @param property
     * @return
     */
    public TableColumn getColumnForProperty(String property) {
        for(TableColumn col : columns) {
            if(col.property.equals(property)) {
                return col;
            }
        }
        return null;
    }

    /**
     * This class describes a JOIN that should be executed for every SELECT statements
     * for a specific class.
     *
     */
    public static class Join {
        public enum JoinType {N_TO_1_JOIN, N_TO_M_JOIN};

        /**
         * The table
         */
        public final String table;

        /**
         * The resource type resulting of this join
         */
        public final String resourceType;

        /**
         * The Column in the *joined* table
         */
        public final String joinedColumn;

        /**
         * The column that is selected
         */
        public final String selectColumn;

        /**
         * The property in the resulting resource
         */
        public final String property;

        /**
         * The inverse property
         */
        public final String inverseProperty;

        /**
         * The join type
         */
        public final JoinType joinType;

        public Join(String table, String resourceType, String joinedColumn,
                String property, String inverseProperty, String selectColumn, JoinType joinType) {
            this.table = table;
            this.resourceType = resourceType;
            this.joinedColumn = joinedColumn;
            this.property = property;
            this.inverseProperty = inverseProperty;
            this.selectColumn = selectColumn == null ? DatabaseConstants.idColumn : selectColumn;
            this.joinType = joinType;
        }
    }

    /**
     * This class describes an actual database column in the database.
     *
     */
    public static class TableColumn {
        /**
         * The columns name
         */
        public final String column;

        /**
         * The column type
         */
        public final SQLColumnType type;

        /**
         * The property that should be used for this column
         */
        public final String property;

        /**
         * The resource type (might be null). Used for N-TO-1 joins.
         */
        public final String resourceType;

        /**
         * If true, then this column is mandatory (NOT NULL)
         */
        public final boolean isMandatory;

        /**
         * Creates a new column for a table definition with the specified attributes
         * @param column
         * @param type
         * @param property
         */
        TableColumn(String column, SQLColumnType type, String property) {
            this.column = column;
            this.type = type;
            this.property = property;
            this.resourceType = null;
            this.isMandatory = true;
        }

        /**
         * Creates a new column for a table definition with the specified attributes
         * @param column
         * @param type
         * @param property
         * @param isMandatory
         */
        TableColumn(String column, SQLColumnType type, String property, boolean isMandatory) {
            this.column = column;
            this.type = type;
            this.property = property;
            this.resourceType = null;
            this.isMandatory = isMandatory;
        }

        /**
         * Creates a new column for a table definition with the specified attributes
         * @param column
         * @param type
         * @param property
         * @param resourceType
         * @param isMandatory
         */
        TableColumn(String column, SQLColumnType type, String property, String resourceType, boolean isMandatory) {
            this.column = column;
            this.type = type;
            this.property = property;
            this.resourceType = resourceType;
            this.isMandatory = isMandatory;
        }
    }

}
