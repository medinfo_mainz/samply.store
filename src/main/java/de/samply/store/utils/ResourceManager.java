/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.utils;

import java.util.ArrayList;
import java.util.List;

import de.samply.store.DatabaseModel;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;

/**
 * A util to save more than one resource at once without starting a new transaction for
 * every resource. Resources are added to a save queue by means of
 * {@link ResourceManager#addResourceToSave(Resource)} and the save operation is executed by
 * {@link ResourceManager#commit()}.
 *
 */
@Deprecated
public class ResourceManager {

    private final DatabaseModel<?> model;

    private final List<Resource> resources = new ArrayList<Resource>();

    /**
     * Creates an instance with a given database model.
     * @param model
     */
    public ResourceManager(DatabaseModel<?> model) {
        this.model = model;
    }

    /**
     * Adds a resource to the save queue.
     * @param resource
     */
    public void addResourceToSave(Resource resource) {
        resources.add(resource);
    }

    /**
     * Removes a resource from the save queue.
     * @param resource
     */
    public void removeResourceToSave(Resource resource) {
        resources.remove(resource);
    }

    /**
     * Clears the save queue without saving any of the queued resources.
     */
    public void reset() {
        resources.clear();
    }

    /**
     * Save the queued resource.
     * @throws DatabaseException If an error occurs while accessing the database.
     */
    public void commit() throws DatabaseException {
        model.beginTransaction();
        for(Resource r : resources) {
            model.saveOrUpdateResource(r);
        }
        model.commit();
    }

}
