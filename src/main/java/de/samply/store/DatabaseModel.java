/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.TablesNotFoundException;
import de.samply.store.history.Transaction;
import de.samply.store.query.ResourceQuery;

/**
 * The interface used to save, get and modify resources.
 *
 * @param <T> the access controller object. This object is used by the access controller only.
 *  The model implementation must ignore this object.
 */
public abstract class DatabaseModel<T> implements AutoCloseable {

    /**
     * Selects the role with the given ID as active role.
     *
     * @param roleid
     * @throws DatabaseException
     */
    abstract public void selectRole(int roleid) throws DatabaseException;

    /**
     * Reloads the given resource. Leaves the given resource instance as it is.
     *
     * @param resource
     * @return the reloaded resource.
     * @throws DatabaseException
     */
    abstract public Resource reloadResource(Resource resource) throws DatabaseException;

    /**
     * Creates a new resource with the given type. Even if the resource type is unknown, this method
     * returns an instance of Resource. Saving this unknown resource type will fail though.
     *
     * @param resourceType the resource type.
     * @return
     */
    abstract public Resource createResource(String resourceType);

    /**
     * Gets the resource with the given type and ID.
     *
     * @param resourceType the resource type. Use your generated Vocabulary.Type.XY to access resource types.
     * @param id ID of the requested resource
     * @return
     * @throws DatabaseException
     */
    abstract public Resource getResource(String resourceType, int id) throws DatabaseException;

    /**
     * Gets the resource which is identified with the given ResourceIdentifier.
     *
     * @param identifier resource identifier string with the format "type:id"
     * @return
     * @throws DatabaseException
     */
    abstract public Resource getResourceByIdentifier(String identifier) throws DatabaseException;

    /**
     * Finds all resources that match the given ResourceQuery
     *
     * @param query
     * @return the list of resources that match the specified ResourceQuery (list may be empty, but not null)
     * @throws DatabaseException
     */
    abstract public ArrayList<Resource> getResources(ResourceQuery query) throws DatabaseException;

    /**
     * Gets all resources with the given type.
     *
     * @param type
     * @return the list of resources that match the specified ResourceQuery (list may be empty, but not null)
     * @throws DatabaseException
     */
    abstract public ArrayList<Resource> getResources(String type) throws DatabaseException;

    /**
     * Saves a new resource or updates an existing resource. A transaction is necessary for this operation,
     * otherwise this method will throw an NoTransactionException.
     *
     * @param resource The resource to save.
     * @throws DatabaseException
     */
    abstract public void saveOrUpdateResource(Resource resource) throws DatabaseException;

    /**
     * Saves a new resource or updates an existing resource. A transaction is necessary for this operation.
     * Convenience method, calls {@link DatabaseModel#saveOrUpdateResource(Resource)} for each argument.
     *
     * @param resources The resources to save.
     * @throws DatabaseException
     */
    public void saveOrUpdateResources(Resource... resources) throws DatabaseException {
        for(Resource r : resources) {
            saveOrUpdateResource(r);
        }
    }

    /**
     * Executes a login.
     *
     * @param user the username
     * @param password the cleartext password
     * @throws DatabaseException
     */
    abstract public void login(String user, String password) throws DatabaseException;

    /**
     * Injects a login with the given username. This is a login method that does not require a valid password.

     * @param user The username of the login to inject.
     * @throws DatabaseException
     */
    abstract public void injectLogin(String user) throws DatabaseException;

    /**
     * Executes a logout.
     */
    abstract public void logout();

    /**
     * Creates a new transaction.
     *
     * @throws DatabaseException
     */
    abstract public void beginTransaction() throws DatabaseException;

    /**
     * Executes a commit.
     *
     * @throws DatabaseException
     */
    abstract public void commit() throws DatabaseException;

    /**
     * Executes a rollback.
     *
     * @throws DatabaseException
     */
    abstract public void rollback() throws DatabaseException;

    /**
     * Creates a new User with the given password. You need to save the returned resource in order to store
     * the new user in the database;
     *
     * @param user The username. Its the applications responsibility to check for a valid username (e.g. exclude special characters).
     * @param password the password. Its the applications responsibility to check for a valid password (e.g. minimum length).
     * @return A resource that represents the created user. This resource has not been saved yet.
     * @throws DatabaseException
     */
    abstract public Resource createUser(String user, String password) throws DatabaseException;

    /**
     * Creates a new user with the given username, password and salt. You need to save the returned resource in order to store
     * the new user in the database;
     */
    abstract public Resource createUser(String username, String password, String salt) throws DatabaseException;

    /**
     * Imports a user with the given username, hashed password and a salt. You need to save the returned resource in order to store
     * the new user in the database. This method if necessary for special use cases. Use with caution.
     *
     * @param username
     * @param hashedPassword use the same hash method as the current configuration, otherwise the login will not work
     * @param salt the salt for the hashed password
     * @return
     * @throws DatabaseException
     */
    abstract public Resource importUser(String username, String hashedPassword, String salt) throws DatabaseException;

    /**
     * Changes the password of the user who is currently logged in.
     *
     * @param resource the user resource
     * @param oldPassword the users old password
     * @param newPassword the users new password
     * @return Returns the first parameter. Does not create a new resource.
     * @throws DatabaseException
     */
    abstract public Resource changeUserPassword(Resource resource, String oldPassword, String newPassword) throws DatabaseException;

    /**
     * Changes the password for the given user
     *
     * @param user the user resource
     * @param newPassword the new cleartext password
     * @return the user resource. It is the same instance as the first argument of this method.
     * @throws DatabaseException
     */
    abstract public Resource changeUserPasswordByAdmin(Resource user, String newPassword) throws DatabaseException;

    /**
     * Generates a new activation code and returns the *unsaved* modified user with the given username. This method does
     * not require a valid login. The returned user resource must not be updated.
     *
     * @param username the username
     * @return the user resource with the new activation code
     * @throws DatabaseException
     */
    abstract public Resource forgotPassword(String username) throws DatabaseException;

    /**
     * Activates the user with the given activationCode (which is unique) and initializes the user
     * with the given password (and activates the user).
     *
     * @param clearTextPassword
     * @param activationCode
     * @throws DatabaseException
     */
    abstract public Resource activateUser(String clearTextPassword, String activationCode) throws DatabaseException;

    /**
     * Sets the AccessController for this DatabaseModel.
     *
     * @param controller
     * @throws DatabaseException
     */
    abstract public void setAccessController(AccessController<T> controller) throws DatabaseException;

    /**
     * Returns the current users username. Null if there was no login.
     *
     * @return
     */
    abstract public String getUser();

    /**
     * Gets the user ID who is currently logged in.
     *
     * @return
     */
    abstract public int getUserId();

    /**
     * Gets all transactions for the given resource.
     *
     * @param resource
     * @return The list of transactions, an empty list if no transactions exist.
     * @throws DatabaseException
     */
    abstract public List<Transaction> getTransactions(Resource resource) throws DatabaseException;

    /**
     * Gets the resource at the time of the specified transaction.
     *
     * @param resource
     * @param transaction
     * @return
     */
    abstract public Resource getResource(Resource resource, Transaction transaction) throws DatabaseException;

    /**
     * Closes the database connection.
     *
     * @throws DatabaseException
     */
    @Override
    abstract public void close() throws DatabaseException;

    /**
     * Executes the given SQL file
     *
     * @param filename
     * @throws DatabaseException
     */
    abstract public void executeFile(String filename) throws DatabaseException;

    /**
     * Executes all SQL statements in the given stream (ideally in UTF-8). The statements are not parsed,
     * they are just passed through to the SQL database.
     *
     * @param reader
     * @throws DatabaseException
     */
    abstract public void executeStream(Reader reader) throws DatabaseException;

    /**
     * Executes all SQL statements in the given string
     *
     * @param sql
     * @throws DatabaseException
     */
    abstract public void executeSQL(String sql) throws DatabaseException;

    /**
     * Returns the current user as resource identifier literal
     *
     * @return
     */
    abstract public ResourceIdentifierLiteral getCurrentUser();

    /**
     * Returns the current AccessController
     *
     * @return
     */
    abstract public AccessController<T> getAccessController();

    /**
     * Returns all resources that meet the given criteria, ignores the access controller.
     * If internal is true the access controller is ignored. Should be used for internal calls only.
     *
     * @param query the resource query.
     * @param internal if true the access controller is ignored for this query
     * @return
     * @throws DatabaseException
     */
    abstract List<Resource> getResources(ResourceQuery query, boolean internal) throws DatabaseException;

    /**
     * Returns the configuration object with the given name.
     *
     * @param name Name of the configuration object.
     * @return the configuration as JSONResource. Returns null, if there is no config with the specified name.
     * @throws DatabaseException
     */
    abstract public JSONResource getConfig(String name) throws DatabaseException;

    /**
     * Saves the configuration objects as "name"
     *
     * @param name Name under which to save the configuration.
     * @param config The configuration to save.
     * @throws DatabaseException
     */
    abstract public void saveConfig(String name, JSONResource config) throws DatabaseException;

    /**
     * Deletes the given resource.
     *
     * @param resource The resource to delete.
     * @throws DatabaseException
     */
    abstract public void deleteResource(Resource resource) throws DatabaseException;

    /**
     * Deletes all given resources.
     * Convenience method, calls {@link DatabaseModel#saveOrUpdateResource(Resource)} for each argument.
     *
     * @param resources The resources to delete.
     * @throws DatabaseException
     */
    public void deleteResources(Resource... resources) throws DatabaseException {
        for(Resource r : resources) {
            deleteResource(r);
        }
    }

    /**
     * Executes the specified action with the specified parameters
     *
     * @param name The action to execute.
     * @param parameters
     * @throws DatabaseException
     */
    abstract public void executeAction(String name, Object... parameters) throws DatabaseException;

    /**
     * Saves or updates the resource, without checking for permission
     *
     * @param resource The resource to save.
     * @throws DatabaseException
     */
    abstract void saveOrUpdateResourceInternal(Resource resource) throws DatabaseException;

    /**
     * Saves or updates the resources without checking for permission. Convenience method, calls
     * {@link DatabaseModel#saveOrUpdateResource(Resource)} for each argument.
     *
     * @param resources The resources to save.
     * @throws DatabaseException
     */
    void saveOrUpdateResourcesInternal(Resource... resources) throws DatabaseException {
        for(Resource r : resources) {
            saveOrUpdateResourceInternal(r);
        }
    }

    /**
     * Checks if the Config table exists.
     *
     * @throws TablesNotFoundException if the Config table does not exist.
     */
    public void testTables() throws TablesNotFoundException {
        try{
            getConfig("test");
        } catch(DatabaseException e) {
            throw new TablesNotFoundException();
        }
    }
}
