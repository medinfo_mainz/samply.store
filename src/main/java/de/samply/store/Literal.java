/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import de.samply.store.sql.SQLColumnType;

/**
 * A Literal is an actual value (a string, a number, a timestamp, etc.), e.g. "Paul" or 5.6
 *
 */
public abstract class Literal<T> extends Value {

    /**
     *
     */
    private static final long serialVersionUID = -6212072735093824167L;

    /**
     * The value
     */
    protected final T value;

    /**
     * Creates an empty instance, i.e. the represented value is null.
     */
    protected Literal() {
        value = null;
    }

    /**
     * Creates an instance from the given value.
     * @param value The value that should be represented by this instance.
     */
    protected Literal(T value) {
        this.value = value;
    }

    /**
     * Gets the represented value.
     */
    public T get() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Literal<?>) {
            return get().equals(((Literal<?>) o).get());
        } else {
            return false;
        }
    }

    @Override
    public abstract String toString();

    /**
     * Returns the corresponding SQL Column Type for this literal.
     */
    public abstract SQLColumnType getCorrespondingSQLType();

}
