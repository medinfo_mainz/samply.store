/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

/**
 * The basic database strings needed for {@link PSQLModel}.
 *
 */
public class BasicDB {

    /** The ID property */
    public static final String ID = "id";

    /**
     * Basic types.
     *
     */
    public class Type {
        public static final String User = "user";
        public static final String Role = "role";
    }

    /**
     * The columns in the "role" table.
     *
     */
    public class Role {
        public static final String Name = "name";
        public static final String RoleType = "roleType";
        public static final String IsUserRole = "isUserRole";
    }

    /**
     * The columns in the "users" table
     *
     */
    public class User {
        public static final String Username = "username";
        public static final String IsActivated = "isActivated";
        public static final String ActivationCode = "activationCode";
        public static final String FailCounter = "failCounter";
        public static final String Password = "password";
        public static final String Salt = "salt";
        public static final String Roles = "roles";

        public class ReadOnly {
            public static final String Contacts = "contacts";
        }
    }
}
