/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.resources;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import de.samply.common.config.FieldType;
import de.samply.common.config.RelationType;
import de.samply.common.config.Resources;
import de.samply.common.config.Resources.ResourceType;
import de.samply.common.config.Resources.ResourceType.Field;
import de.samply.common.config.Resources.ResourceType.Relation;
import de.samply.store.DatabaseConstants;
import de.samply.store.DatabaseDefinitions;
import de.samply.store.TableDefinition;
import de.samply.store.sql.SQLColumnType;
import de.samply.string.util.StringUtil;

/**
 * Loads the resource definition file and parses it.
 * Can create DDL SQL statements, load the DatabaseDefinitions and
 * create static vocabulary Java files.
 *
 */
public class Configuration {

    private static Configuration instance = null;

    /**
     * Loads the resource definitions from the specified path.
     * @param path Path to file with resource definitions.
     * @throws FileNotFoundException If the specified file is not found.
     * @throws JAXBException If the specified file is not deserializable.
     */
    public static void parseResourceConfig(String path) throws FileNotFoundException, JAXBException {
        parseResourceConfig(new FileInputStream(path));
    }

    /**
     * Loads the resource definitions from the specified path.
     *
     * @param file File with resource definitions.
     * @throws FileNotFoundException If the specified file is not found.
     * @throws JAXBException If the specified file is not deserializable.
     */
    public static void parseResourceConfig(File file) throws FileNotFoundException, JAXBException {
        parseResourceConfig(file.getAbsolutePath());
    }

    /**
     * Loads the resource definitions from the specified input stream.
     *
     * @param stream Input stream from which to read the resource definitions.
     * @throws JAXBException If the specified file is not deserializable.
     */
    public static void parseResourceConfig(InputStream stream) throws JAXBException {
        instance = new Configuration(stream);
        instance.generateDefinitions();
    }

    /**
     * Returns the static Configuration instance.
     */
    public static Configuration get() {
        return instance;
    }

    /**
     * The list of parsed relations.
     */
    private ArrayList<ParsedRelation> relations = new ArrayList<ParsedRelation>();

    /**
     * The map that store the resource config for a specific resource type.
     */
    private HashMap<String, ResourceConfig> configs = new HashMap<>();

    /**
     * Initializes the Configuration by defining all *default* resources.
     */
    private Configuration() {
        /**
         * Generate the *default* tables consisting of transactions and users only.
         */

        ResourceType resourceType = new ResourceType();
        resourceType.setResource(false);
        resourceType.setVersioned(false);
        resourceType.setName("transaction");

        Relation relation = new Relation();
        relation.setType(RelationType.N_TO_1);
        relation.setTo("user");
        resourceType.getRelation().add(relation);

        Field field = new Field();
        field.setType(FieldType.TIMESTAMP);
        field.setName("timestamp");

        resourceType.getField().add(field);
        parseResourceType(resourceType);

        resourceType = new ResourceType();
        resourceType.setName("user");

        field = new Field();
        field.setType(FieldType.STRING);
        field.setName("username");
        field.setUnique(true);
        field.setMaxLength(30);
        resourceType.getField().add(field);

        field = new Field();
        field.setType(FieldType.STRING);
        field.setName("password");
        resourceType.getField().add(field);

        field = new Field();
        field.setType(FieldType.STRING);
        field.setName("salt");
        resourceType.getField().add(field);

        field = new Field();
        field.setType(FieldType.STRING);
        field.setName("activationCode");
        resourceType.getField().add(field);

        field = new Field();
        field.setType(FieldType.INTEGER);
        field.setName("failCounter");
        resourceType.getField().add(field);

        field = new Field();
        field.setType(FieldType.INTEGER);
        field.setName("isActivated");
        resourceType.getField().add(field);
        parseResourceType(resourceType);

        resourceType = new ResourceType();
        resourceType.setName("config");
        resourceType.setResource(false);
        resourceType.setVersioned(false);

        field = new Field();
        field.setType(FieldType.JSON);
        field.setName("data");
        resourceType.getField().add(field);

        field = new Field();
        field.setType(FieldType.STRING);
        field.setUnique(true);
        field.setName("name");
        resourceType.getField().add(field);
        parseResourceType(resourceType);
    }

    /**
     * Loads the resource definitions from the specified path.
     *
     * @param configPath Path to file with resource definitions.
     * @throws FileNotFoundException If the specified file is not found.
     * @throws JAXBException if the specified file is not serializable.
     */
    public Configuration(String configPath) throws FileNotFoundException, JAXBException {
        this();
        this.parseResourceConfiguration(new File(configPath));
    }

    /**
     * Loads the resource definitions from the specified input stream.
     *
     * @param stream Input stream from which to read the resource definitions.
     * @throws JAXBException if the specified file is not serializable.
     */
    public Configuration(InputStream stream) throws JAXBException {
        this();
        this.parseTableFile(stream);
    }

    /**
     * Loads the resource definitions from the specified Resources instance.
     *
     * @param resources Resource from which to read the resource definitions.
     * @throws JAXBException if the specified file is not serializable.
     */
    public Configuration(Resources resources) throws JAXBException {
        this();
        this.parseResources(resources);
    }

    /**
     * Checks if there is a ResourceConfig for the specified type. If there is none,
     * an instance is created and returned.
     *
     * @param name resource type name
     * @param resourceType
     * @return
     */
    private ResourceConfig get(String name, ResourceType resourceType) {
        if(!configs.containsKey(name)) {
            configs.put(name, new ResourceConfig(name, resourceType));
        }
        return configs.get(name);
    }

    /**
     * Generates the DDL SQL statements and returns them in one string.
     */
    public String generateSQL() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);

        generateSQL(ps);

        try {
            return baos.toString("UTF8");
        } catch (UnsupportedEncodingException e) {
            return "/* Error in Encoding: Not supported! */";
        }
    }

    /**
     * Generates SQL statements that create all tables, indeces, etc.
     *
     * @param out Stream to which the statements are written.
     */
    public void generateSQL(PrintStream out) {
        out.println("BEGIN;");

        out.println("CREATE OR REPLACE FUNCTION resourceID(input text) RETURNS INTEGER AS");
        out.println("$$");
        out.println("SELECT split_part(input, ':', 2)::INTEGER");
        out.println("$$ LANGUAGE SQL STRICT;");

        out.println("CREATE OR REPLACE FUNCTION resourceType(input text) RETURNS TEXT AS");
        out.println("$$");
        out.println("    SELECT split_part(input, ':', 1):: TEXT");
        out.println("$$ LANGUAGE SQL STRICT;");

        out.println("SET CONSTRAINTS ALL DEFERRED;");

        for(ResourceConfig resConfig : configs.values()) {
            out.println("DROP TABLE IF EXISTS \"" + Helper.getTableName(resConfig) + "\" CASCADE;");

            if(resConfig.et.isVersioned()) {
                out.println("DROP TABLE IF EXISTS \"" + Helper.getTableName(resConfig, true) + "\" CASCADE;");
            }
        }

        for(ParsedRelation relation : relations) {
            if(relation.type == RelationType.N_TO_M) {
                out.println("DROP TABLE IF EXISTS \"" + Helper.getTableName(relation, false) + "\" CASCADE;");
                out.println("DROP TABLE IF EXISTS \"" + Helper.getTableName(relation, true) + "\" CASCADE;");
            }
        }

        for(ResourceConfig resConfig : configs.values()) {
            out.println("CREATE TABLE \"" + Helper.getTableName(resConfig) + "\" (");

            out.println(StringUtil.join(constructFieldParts(resConfig, false), ",\n"));
            out.println(");");

            if(resConfig.et.isVersioned()) {
                out.println("CREATE TABLE \"" + Helper.getTableName(resConfig, true) + "\" (");
                out.println("\t\"oid\" INTEGER NOT NULL,");
                out.println(StringUtil.join(constructFieldParts(resConfig, true), ",\n"));
                out.println(");");
            }
        }

        for(ResourceConfig t : configs.values()) {
            if(t.et.isVersioned()) {
                out.println("ALTER TABLE \"" + Helper.getTableName(t) + "\" ADD FOREIGN KEY (\"transaction_id\") "
                        + "REFERENCES \"transactions\" (\"id\");");
            }
        }

        for(ParsedRelation relation : relations) {
            ResourceConfig resConfigFrom = configs.get(relation.from);
            ResourceConfig resConfigTo = configs.get(relation.to);

            if(relation.type == RelationType.N_TO_1) {
                out.println("ALTER TABLE \"" + Helper.getTableName(resConfigFrom)
                        + "\" ADD FOREIGN KEY (\"" + Helper.getForeignKeyColumn(relation) + "\")"
                        + " REFERENCES \"" + Helper.getTableName(resConfigTo) + "\" (\"id\") " +
                            (relation.onDeleteCascade ? "ON DELETE CASCADE" : "") + ";");
                out.println("CREATE INDEX ON \"" + Helper.getTableName(resConfigFrom)
                        + "\" (\"" + Helper.getForeignKeyColumn(relation) + "\");");
            } else {
                out.println("CREATE TABLE \"" + Helper.getTableName(relation, false) + "\" (\n"
                        + 	(resConfigFrom.et.isVersioned() || resConfigTo.et.isVersioned() ?
                                "\t\"transaction_id\" INTEGER NOT NULL REFERENCES \"transactions\" (\"id\"),\n" : "" )
                        + "\t\"" + Helper.getForeignKeyColumn(relation.from) + "\" INTEGER NOT NULL REFERENCES \"" + Helper.getTableName(resConfigFrom) + "\" (\"id\") "
                                + (relation.onDeleteCascade ? "ON DELETE CASCADE" : "") + ",\n"
                        + "\t\"" + Helper.getForeignKeyColumn(relation.to) + "\" INTEGER NOT NULL REFERENCES \"" + Helper.getTableName(resConfigTo) + "\" (\"id\") "
                                + (relation.onDeleteCascade ? "ON DELETE CASCADE" : "") + ",\n"
                        + "\tPRIMARY KEY (\"" + Helper.getForeignKeyColumn(relation.from) + "\", \"" + Helper.getForeignKeyColumn(relation.to) + "\")\n"
                        + ");");

                out.println("CREATE TABLE \"" + Helper.getTableName(relation, true) + "\" (\n"
                        + 	(resConfigFrom.et.isVersioned() || resConfigTo.et.isVersioned() ?
                                "\t\"transaction_id\" INTEGER NOT NULL REFERENCES \"transactions\" (\"id\"),\n" : "" )
                        + "\t\"" + Helper.getForeignKeyColumn(relation.from) + "\" INTEGER NOT NULL,\n"
                        + "\t\"" + Helper.getForeignKeyColumn(relation.to) + "\" INTEGER NOT NULL,\n"
                        + "\tPRIMARY KEY (\"" + Helper.getForeignKeyColumn(relation.from) + "\", \"" + Helper.getForeignKeyColumn(relation.to) + "\", \"transaction_id\")\n"
                        + ");");
            }
        }

        out.println("COMMIT;");
    }

    /**
     * Construct the fields (columns) for a specific resource config.
     *
     * @param resourceConfig
     * @param isHistory
     * @return
     */
    private ArrayList<String> constructFieldParts(ResourceConfig resourceConfig, boolean isHistory) {
        ArrayList<String> parts = new ArrayList<>();

        parts.add("\t\"id\" SERIAL PRIMARY KEY");
        if(resourceConfig.et.isResource()) {
            if(DatabaseConstants.PSQLVersionCode >= 2) {
                parts.add("\t\"data\" JSONB");
            } else {
                parts.add("\t\"data\" JSON");
            }
        }

        for(Field field : resourceConfig.fields.values()) {
            String sqlType = convertFieldType(field);
            StringBuilder builder = new StringBuilder("\t\"").append(field.getName())
                    .append("\" ").append(sqlType);

            if(field.isUnique() && !isHistory) {
                builder.append(" UNIQUE");
            }

            if(field.isMandatory()) {
                builder.append(" NOT NULL");
            }

            if(!field.getDefault().equals("")) {
                builder.append(" DEFAULT '").append(field.getDefault()).append("'");
            }

            parts.add(builder.toString());
        }

        for(ParsedRelation rt : relations) {
            if(rt.type == RelationType.N_TO_1 && rt.from.equals(resourceConfig.name)) {
                parts.add("\t\"" + Helper.getForeignKeyColumn(rt) + "\" INTEGER NOT NULL");
            }
        }

        if(resourceConfig.et.isVersioned()) {
            parts.add("\t\"transaction_id\" INTEGER NOT NULL");
        }

        return parts;
    }

    /**
     * Generates the VocabularyClass used for quick-access.
     *
     * @param className Name of the generated class.
     * @param pkg Package of the generated class.
     * @param out Stream to which the class definition is written.
     */
    public void generateOntologyJava(String className, String pkg, PrintStream out) {
        out.println("package " + pkg + ";");
        out.println("");
        out.println("public class " + className + " {");

        out.println("");
        out.println("\tpublic static final String ID = \"id\";");
        out.println("");
        out.println("\tpublic class Type {");
        for(ResourceConfig resConfig : configs.values()) {
            out.println("\t\tpublic static final String " + capitalize(resConfig.name) + " = \"" + resConfig.name + "\";");
        }
        out.println("\t}");
        out.println("");

        for(ResourceConfig resConfig : configs.values()) {
            if(!resConfig.et.isResource()) {
                continue;
            }
            out.println(""); // Just a spaceer
            out.println("\tpublic class " + capitalize(resConfig.name) + " {");
            for(Field ft : resConfig.fields.values()) {
                out.println("\t\tpublic static final String " + capitalize(ft.getName()) + " = \"" + ft.getName() + "\";");
            }

            /**
             * Find n_to_m and n_to_1 relations
             */
            for(ParsedRelation relation : relations) {
                if(relation.type == RelationType.N_TO_M) {
                    if(relation.from.equals(resConfig.name)) {
                        String plural = Helper.getPlural(relation.to);
                        out.println("\t\tpublic static final String " + capitalize(plural) + " = \"" + plural + "\";");
                    }
                    if(relation.to.equals(resConfig.name)) {
                        String plural = Helper.getPlural(relation.from);
                        out.println("\t\tpublic static final String " + capitalize(plural) + " = \"" + plural + "\";");
                    }
                } else {
                    if(relation.from.equals(resConfig.name)) {
                        if(relation.name == null) {
                            out.println("\t\tpublic static final String " + capitalize(relation.to) + " = \"" + relation.to + "\";");
                        } else {
                            out.println("\t\tpublic static final String " + capitalize(relation.name) + " = \"" + relation.name + "\";");
                        }
                    }
                }
            }
            out.println("");
            out.println("\t\tpublic class ReadOnly {");
            for(ParsedRelation relation : relations) {
                ResourceConfig cFrom = configs.get(relation.from);
                if (relation.type == RelationType.N_TO_1) {
                    if (relation.to.equals(resConfig.name) && cFrom.et.isResource()) {
                        if(relation.name == null) {
                            String plural = Helper.getPlural(relation.from);
                            out.println("\t\t\tpublic static final String " + capitalize(plural) + " = \"" + plural + "\";");
                        } else {
                            out.println("\t\t\tpublic static final String " + capitalize(relation.name) + " = \"" + relation.name + "\";");
                        }
                    }
                }
            }
            out.println("\t\t}");
            out.println("\t}");
        }

        out.println("}");
    }

    /**
     * Capitalize a string.
     * @param input String to capitalize.
     * @return A copy of input with the first letter capizalized.
     */
    private String capitalize(String input) {
        return Character.toUpperCase(input.charAt(0)) + input.substring(1);
    }

    @SuppressWarnings("unused")
    @Deprecated
    private String getSingleName(String plural) {
        String withoutS = plural.substring(0, plural.length() - 1);
        if(withoutS.endsWith("ie")) {
            withoutS = withoutS.substring(0, withoutS.length() - 2) + "y";
        }
        return withoutS;
    }

    /**
     * Parses the resource configuration in the given file.
     * @param file
     * @throws JAXBException
     * @throws FileNotFoundException
     */
    private void parseResourceConfiguration(File file) throws JAXBException, FileNotFoundException {
        if(!file.exists()) {
            System.out.println("Error: File " + file.getAbsolutePath() + " does not exist!");
        }

        parseTableFile(new FileInputStream(file));
    }

    /**
     * Parses the resource configuration in the given stream.
     * @param stream
     * @throws JAXBException
     */
    private void parseTableFile(InputStream stream) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Resources.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        Resources dt = (Resources) unmarshaller.unmarshal(stream);
        parseResources(dt);
    }

    /**
     * Parses the given resources.
     * @param resources
     */
    private void parseResources(Resources resources) {
        for (ResourceType r : resources.getResourceType()) {
            parseResourceType(r);
        }
    }

    private void parseResourceType(ResourceType resourceType) {
        ResourceConfig config = get(resourceType.getName(), resourceType);
        for (Field field : resourceType.getField()) {
            config.addField(field);
        }

        for(Relation rt : resourceType.getRelation()) {
            relations.add(new ParsedRelation(rt, resourceType));
        }
    }

    /**
     * Converts the given field into the proper SQL column type.
     * @param ftype
     * @return
     */
    private String convertFieldType(Field ftype) {
        FieldType e = ftype.getType();
        switch(e) {
        case STRING:
            if(ftype.getMaxLength() > 0) {
                return "VARCHAR(" + ftype.getMaxLength() + ")";
            } else {
                return "TEXT";
            }

        case INTEGER:
            return "INTEGER";

        case FLOAT:
            return "FLOAT";

        case TIMESTAMP:
            return "TIMESTAMP";

        case JSON:
            return DatabaseConstants.getSQLJson();

        default:
            throw new RuntimeException("Not valid FieldType: " + ftype.toString());
        }
    }

    /**
     * Fills the DatabaseDefinitions singleton.
     */
    private void generateDefinitions() {
        DatabaseDefinitions.clear();

        for(ResourceConfig resConfig : configs.values()) {
            if(!resConfig.et.isResource()) {
                continue;
            }

            TableDefinition tableDef = DatabaseDefinitions.get(resConfig.name);
            if(tableDef == null) {
                tableDef = new TableDefinition(Helper.getTableName(resConfig), resConfig.name);
            }

            for(Field field : resConfig.fields.values()) {
                tableDef.createField(field.getName(), SQLColumnType.valueOf(field.getType().toString().toUpperCase()), field.getName(),
                        field.isMandatory());
            }

            for(ParsedRelation relation : relations) {
                ResourceConfig fromCfg = configs.get(relation.from);
                ResourceConfig toCfg = configs.get(relation.to);
                if(!fromCfg.et.isResource() || !toCfg.et.isResource()) {
                    continue;
                }

                if(relation.type == RelationType.N_TO_1) {
                    ResourceConfig cfrom = configs.get(relation.from);

                    if(relation.from.equals(resConfig.name)) {
                        if(relation.name == null) {
                            tableDef.createField(Helper.getForeignKeyColumn(relation), SQLColumnType.RESOURCE, relation.to, relation.to, true);
                        } else {
                            tableDef.createField(Helper.getForeignKeyColumn(relation), SQLColumnType.RESOURCE, relation.name, relation.to, true);
                        }
                    }

                    if(relation.to.equals(resConfig.name)) {
                        if(relation.name == null) {
                            tableDef.createJoin(Helper.getTableName(cfrom), relation.from, Helper.getForeignKeyColumn(relation), Helper.getPlural(relation.from), relation.to);
                        } else {
                            tableDef.createJoin(Helper.getTableName(cfrom), relation.from, Helper.getForeignKeyColumn(relation), relation.name, relation.to);
                        }
                    }
                } else {
                    if(relation.to.equals(resConfig.name)) {
                        tableDef.createJoin(Helper.getTableName(relation, false), relation.from, Helper.getForeignKeyColumn(relation),
                                Helper.getPlural(relation.from), Helper.getPlural(relation.to), Helper.getForeignKeyColumn(relation, true));
                    }

                    if(relation.from.equals(resConfig.name)) {
                        tableDef.createJoin(Helper.getTableName(relation, false), relation.to, Helper.getForeignKeyColumn(relation, true),
                                Helper.getPlural(relation.to), Helper.getPlural(relation.from), Helper.getForeignKeyColumn(relation));
                    }
                }
            }

            DatabaseDefinitions.put(resConfig.name, tableDef);
        }
    }

    /**
     * Represents a parsed relation.
     *
     */
    public class ParsedRelation {

        public final String to;
        public final RelationType type;
        public final String from;
        public final boolean onDeleteCascade;
        public final String name;

        public ParsedRelation(Relation rt, ResourceType et) {
            this.to = rt.getTo();
            this.type = rt.getType();
            this.onDeleteCascade = rt.isOnDeleteCascade();
            this.from = et.getName();
            this.name = rt.getName();
        }

    }
}
