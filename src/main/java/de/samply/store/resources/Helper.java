/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.resources;

import de.samply.common.config.RelationType;
import de.samply.common.config.Resources.ResourceType;
import de.samply.store.resources.Configuration.ParsedRelation;

/**
 * Utility class with several methods regarding database tables.
 *
 */
public class Helper {

    public static String getTableName(ResourceConfig cfg) {
        return getTableName(cfg.et, false);
    }

    public static String getTableName(ResourceType et) {
        return getTableName(et, false);
    }

    /**
     * Returns the table name for the given resource type.
     * @param et
     * @param history
     * @return
     */
    public static String getTableName(ResourceType et, boolean history) {
        return getPlural(et.getName()) + (history ? "_history" : "");
    }

    /**
     * Returns the table name for the given resource type.
     * @param cfg
     * @param history
     * @return
     */
    public static String getTableName(ResourceConfig cfg, boolean history) {
        return getTableName(cfg.et, history);
    }

    /**
     * Generates the plural form of the given input. Does not work with irregular words like
     * children.
     * @param input
     * @return
     */
    public static String getPlural(String input) {
        if(input.endsWith("y")) {
            return input.substring(0, input.length() - 1) + "ies";
        } else if(input.endsWith("s")) {
            return input + "es";
        } else 	{
            return input + "s";
        }
    }

    /**
     * Returns the table name for the given n-to-m relation.
     * @param r
     * @param history
     * @return
     */
    public static String getTableName(ParsedRelation r, boolean history) {
        if(r.type == RelationType.N_TO_M) {
            return getPlural(r.from) + "_" + getPlural(r.to) + (history ? "_history" : "");
        } else {
            throw new RuntimeException("Invalid Table for n_to_1 relation!");
        }
    }

    /**
     * Returns the foreign key column of the given relation.
     * @param r
     * @return
     */
    public static String getForeignKeyColumn(ParsedRelation r) {
        if(r.name == null) {
            return r.to + "_id";
        } else {
            return r.name;
        }
    }

    /**
     * Returns the reverse foreign key column of the given relation.
     * @param r
     * @param inverse
     * @return
     */
    public static String getForeignKeyColumn(ParsedRelation r, boolean inverse) {
        if(inverse) {
            return r.from + "_id";
        } else {
            return r.to + "_id";
        }
    }

    /**
     * Returns the foreign key column for the given column.
     * @param input
     * @return
     */
    public static String getForeignKeyColumn(String input) {
        return input + "_id";
    }

}
