/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.query;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.store.BasicDB;
import de.samply.store.BooleanLiteral;
import de.samply.store.DatabaseConstants;
import de.samply.store.DatabaseDefinitions;
import de.samply.store.Identifiable;
import de.samply.store.Literal;
import de.samply.store.NumberLiteral;
import de.samply.store.RangeLiteral;
import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.StringLiteral;
import de.samply.store.TableDefinition;
import de.samply.store.TableDefinition.Join;
import de.samply.store.TableDefinition.Join.JoinType;
import de.samply.store.TableDefinition.TableColumn;
import de.samply.store.TimestampLiteral;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.sql.Clauses;
import de.samply.store.sql.SQLBooleanValue;
import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLColumnType;
import de.samply.store.sql.SQLFloatValue;
import de.samply.store.sql.SQLIntValue;
import de.samply.store.sql.SQLLongValue;
import de.samply.store.sql.SQLQuery;
import de.samply.store.sql.SQLRangeValue;
import de.samply.store.sql.SQLStringValue;
import de.samply.store.sql.SQLTable;
import de.samply.store.sql.SQLTemporaryTable;
import de.samply.store.sql.SQLTimestampValue;
import de.samply.store.sql.SQLValue;
import de.samply.store.sql.clauses.AndClause;
import de.samply.store.sql.clauses.Clause;
import de.samply.store.sql.clauses.EqualClause;
import de.samply.store.sql.clauses.ListClause;
import de.samply.store.sql.clauses.OrClause;
import de.samply.store.sql.functions.SQLResourceID;
import de.samply.store.sql.functions.SQLResourceType;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * Use this class to search for resources that meet a set of criterias.
 *
 */
public class ResourceQuery {

    private static Logger logger = LogManager.getLogger(ResourceQuery.class);

    /**
     * The list of predefined relations. Those relations are prioritized.
     */
    private ArrayList<Relation> relations = new ArrayList<>();

    /**
     * The list of criterias
     */
    private ArrayList<Criteria> criterias = new ArrayList<>();

    private HashMap<String, SQLTemporaryTable> temporaryTables = new HashMap<>();

    /**
     * The SQL query that is being constructed.
     */
    private SQLQuery sqlQuery;

    /**
     * The result resource type.
     */
    public final String resultType;

    public List<Criteria> getAll() {
        return Collections.unmodifiableList(criterias);
    }

    /**
     * Adds a new EqualCriteria for the resultType. Equal to add(Criteria.Equal(resultType, property, value));
     * @param property
     * @param value
     * @return
     */
    public ResourceQuery addEqual(String property, Value value) {
        add(Criteria.Equal(resultType, property, value));
        return this;
    }

    /**
     * Adds a new EqualCriteria for the resultType. Equal to add(Criteria.Equal(resultType, property, value));
     * @param property
     * @param value
     * @return
     */
    public ResourceQuery addEqual(String property, Number value) {
        addEqual(property, new NumberLiteral(value));
        return this;
    }

    /**
     * Adds a new EqualCriteria for the resultType. Equal to add(Criteria.Equal(resultType, property, value));
     * @param property
     * @param value
     * @return
     */
    public ResourceQuery addEqual(String property, Boolean value) {
        addEqual(property, new BooleanLiteral(value));
        return this;
    }

    /**
     * Adds a new EqualCriteria for the resultType. Equal to add(Criteria.Equal(resultType, property, value));
     * @param property
     * @param value
     * @return
     */
    public ResourceQuery addEqual(String property, String value) {
        addEqual(property, new StringLiteral(value));
        return this;
    }

    /**
     * Adds a new criteria
     * @param criteria
     */
    public void add(Criteria criteria) {
        criterias.add(criteria);
    }

    /**
     * Creates an empty resource query for the given resource type.
     * @param resourceType
     */
    public ResourceQuery(String resourceType) {
        this.resultType = resourceType;
    }

    /**
     * Prepares a SQLQuery for this ResourceQuery
     * @return
     * @throws DatabaseException
     */
    public SQLQuery prepareSQLCriteria() throws DatabaseException {
        TableDefinition tdef = DatabaseDefinitions.get(resultType);
        sqlQuery = new SQLQuery(tdef.getTable());

        SQLColumn idColumn = sqlQuery.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        sqlQuery.addSelection(idColumn);

        for(Relation r : relations) {
            handleRelation(r);
        }

        for(Criteria c : criterias) {
            sqlQuery.addClause(convertCriteria(c));
        }

        return sqlQuery;
    }

    /**
     * Handles the specified relation. It adds the necessary joins to the SQLQuery.
     * @param relation
     * @throws InvalidOperationException
     */
    private void handleRelation(Relation relation) throws InvalidOperationException {
        SQLValue colFrom = null;
        SQLColumn colTo = null;

        SQLTable tableFrom = null;

        TableDefinition firstDef = DatabaseDefinitions.get(relation.resourceType);

        SQLTable joinTable = null;
        Clause additionalClause = null;

        try {
            tableFrom = findTable(relation.resourceType);
        } catch(NoPathFoundException e) {
            tableFrom = sqlQuery.getTable(firstDef.getTable());
            joinTable = tableFrom;
        }

        TableColumn tableColumn = firstDef.getColumnForProperty(relation.property);

        if(tableColumn != null) {
            colFrom = tableFrom.getColumn(tableColumn.column, tableColumn.type);
        } else {
            SQLColumn c = tableFrom.getColumn(DatabaseConstants.dataColumn, SQLColumnType.JSON_STRING, relation.property);
            colFrom = new SQLResourceID(c);
            additionalClause = Clauses.Equal(new SQLResourceType(c), new SQLStringValue(relation.resourceTypeTo));
        }

        try {
            SQLTable tableTo = findTable(relation.resourceTypeTo);
            colTo = tableTo.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        } catch(NoPathFoundException e) {
            TableDefinition def = DatabaseDefinitions.get(relation.resourceTypeTo);
            joinTable = sqlQuery.getTable(def.getTable());
            colTo = joinTable.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        }

        // If this relation is defined in the resources.xml, we just skip it here. findTable handles this
        // kind of relation
        if(joinTable != null) {
            sqlQuery.addJoin(joinTable, colFrom, colTo);

            if(additionalClause != null) {
                sqlQuery.addClause(additionalClause);
            }
        }
    }

    /**
     * Adds a prioritized n-to-1 relation
     * @param type
     * @param typeProperty
     * @param typeTo
     */
    public void addRelation(String type, String typeProperty, String typeTo) {
        relations.add(new Relation(type, typeProperty, typeTo, true));
    }

    /**
     * Adds a prioritized relation
     * @param type
     * @param typeProperty
     * @param typeTo
     * @param nTo1
     */
    public void addRelation(String type, String typeProperty, String typeTo, boolean nTo1) {
        relations.add(new Relation(type, typeProperty, typeTo, nTo1));
    }

    /**
     * Converts a resource criteria into the corresponding SQL clause
     * @param criteria
     * @return
     * @throws DatabaseException
     */
    private Clause convertCriteria(Criteria criteria) throws DatabaseException {
        if(criteria instanceof ValueCriteria) {
            ValueCriteria valueCriteria = (ValueCriteria) criteria;

            Literal<?> literal = null;

            if(valueCriteria.value instanceof Literal<?>) {
                literal = (Literal<?>) valueCriteria.value;
            } else if(valueCriteria.value instanceof Identifiable) {
                Identifiable identifiable = (Identifiable) valueCriteria.value;
                literal = new NumberLiteral(identifiable.getId());
            } else {
                throw new InvalidOperationException("Invalid value in ValueCriteria: " + valueCriteria.value.toString());
            }

            SQLColumnType sqlType = literal.getCorrespondingSQLType();
            SQLColumn sqlCol = findSQLColumn(valueCriteria.type, valueCriteria.property, sqlType);
            SQLValue value = convertValue(valueCriteria.value, sqlCol.type);

            return valueCriteria.getClause(sqlCol, value);
        } else if(criteria instanceof ListCriteria) {
            ListClause listClause = null;
            List<Clause> clauses = new ArrayList<Clause>();

            for(Criteria c : ((ListCriteria) criteria).getAll()) {
                clauses.add(convertCriteria(c));
            }

            if(criteria instanceof OrCriteria) {
                listClause = new OrClause(clauses);
            } else if(criteria instanceof AndCriteria) {
                listClause = new AndClause(clauses);
            }
            return listClause;
        } else if(criteria instanceof PropertyCriteria) {
            PropertyCriteria propertyCriteria = (PropertyCriteria) criteria;
            SQLColumn sqlCol = findSQLColumn(propertyCriteria.type, propertyCriteria.property, SQLColumnType.JSON_OBJ);
            return propertyCriteria.getClause(sqlCol);
        } else if(criteria instanceof ArrayValueCriteria) {
            ArrayValueCriteria arrayCriteria = (ArrayValueCriteria) criteria;
            SQLColumn jsonColumn = getJsonArrayColumn(arrayCriteria);

            if(arrayCriteria.getCriteria() instanceof ValueCriteria) {
                ValueCriteria vc = (ValueCriteria) arrayCriteria.getCriteria();

                if(vc.value instanceof Literal<?>) {
                    Literal<?> literal = (Literal<?>) vc.value;
                    return vc.getClause(jsonColumn, convertValue(vc.value, literal.getCorrespondingSQLType()));
                } else {
                    throw new UnsupportedOperationException("Unknown value: " + vc.value.getClass().getCanonicalName());
                }
            } else if(arrayCriteria.getCriteria() instanceof PropertyCriteria) {
                PropertyCriteria propCriteria = arrayCriteria.getCriteria();
                return propCriteria.getClause(jsonColumn);
            } else {
                throw new UnsupportedOperationException("Unknown Criteria: " + arrayCriteria.getCriteria().getClass().getCanonicalName());
            }
        } else {
            throw new UnsupportedOperationException("Unknown Criteria: " + criteria.getClass().getCanonicalName());
        }
    }

    /**
     * @param arrayCriteria
     * @return
     * @throws NoPathFoundException
     * @throws InvalidOperationException
     */
    private SQLColumn getJsonArrayColumn(ArrayValueCriteria arrayCriteria) throws InvalidOperationException, NoPathFoundException {
        if(!temporaryTables.containsKey(getArrayRecord(arrayCriteria))) {
            /**
             * Is the json temporary table that is required for this array value criteria has not been joined yet,
             * do so and store the temporary table in the hashmap.
             */
            SQLTable outerTable = findTable(arrayCriteria.getFormType());
            SQLColumn outerIdColumn = outerTable.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);

            SQLQuery query = new SQLQuery(DatabaseDefinitions.get(arrayCriteria.getFormType()).table);
            SQLColumn innerIdColumn = query.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
            SQLColumn jsonArrayColumn = query.getMainTable().getJsonArrayColumn(DatabaseConstants.dataColumn, arrayCriteria.getRecordUrn());

            query.addSelection(jsonArrayColumn);
            query.addSelection(innerIdColumn);

            query.addClause(new EqualClause(query.getMainTable().getColumn("name", SQLColumnType.STRING), new SQLStringValue(arrayCriteria.getFormName())));

            SQLTemporaryTable temporaryTable = new SQLTemporaryTable(query, sqlQuery);

            sqlQuery.addJoin(temporaryTable, temporaryTable.getOutsideColumn(innerIdColumn), outerIdColumn);

            temporaryTables.put(getArrayRecord(arrayCriteria),
                    temporaryTable);
        }

        SQLTemporaryTable table = temporaryTables.get(getArrayRecord(arrayCriteria));

        return table.getOutsideColumn(table.getSql().getMainTable().getJsonArrayColumn(DatabaseConstants.dataColumn, arrayCriteria.getRecordUrn()),
                arrayCriteria.getSqlColumnType(), arrayCriteria.getCriteria().property);
    }

    /**
     * Returns the string that identifies the array value criteria.
     * @param arrayCriteria
     * @return
     */
    private String getArrayRecord(ArrayValueCriteria arrayCriteria) {
        return arrayCriteria.getFormType() + "_" + arrayCriteria.getFormName() + "_" + arrayCriteria.getRecordUrn();
    }

    /**
     * This method finds the correct SQLColumn for the given type
     * and property. If the table is not available, it will find a "path".
     * @param sqlQuery
     * @param type
     * @param property
     * @param sqlColType
     * @return
     * @throws DatabaseException
     */
    private SQLColumn findSQLColumn(String type, String property, SQLColumnType sqlColType) throws DatabaseException {
        TableDefinition def = DatabaseDefinitions.get(type);
        SQLTable table = findTable(type);

        TableColumn col = def.getColumnForProperty(property);

        /**
         * The id column is not defined in the table definitions.
         * If the property is "id", we just get the SQLColumn from id
         * properly
         */
        if(property.equals(BasicDB.ID)) {
            return table.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        }

        if(col != null) {
            return table.getColumn(col.column, col.type);
        } else {
            return table.getColumn(DatabaseConstants.dataColumn, sqlColType, property);
        }
    }

    /**
     * Converts the given Value into the corresponding SQLValue
     * @param value
     * @param type
     * @return
     * @throws DatabaseException
     */
    private SQLValue convertValue(Value value, SQLColumnType type) throws DatabaseException {
        switch (type) {
        case FLOAT:
        case JSON_FLOAT:
            return new SQLFloatValue(value.asFloat());

        case INTEGER:
        case JSON_INT:
            if (value instanceof RangeLiteral) {
                return new SQLRangeValue(((RangeLiteral) value).from, ((RangeLiteral) value).to);
            } else {
                return new SQLIntValue(value.asInteger());
            }

        case JSON_LONG:
        case LONG:
            return new SQLLongValue(value.asLong());

        case STRING:
        case JSON_STRING:
            return new SQLStringValue(value.getValue());

        case TIMESTAMP:
        case JSON_TIMESTAMP:
            if (value instanceof TimestampLiteral) {
                return new SQLTimestampValue(value.asTimestamp());
            } else {
                throw new InvalidOperationException(
                        "Trying to access a timestamp column without"
                                + " a TimestampLiteral!");
            }

        case BOOLEAN:
        case JSON_BOOLEAN:
            return new SQLBooleanValue(value.asBoolean());

        case RESOURCE:
            if(value instanceof NumberLiteral) {
                return new SQLIntValue(value.asInteger());
            } else if(value instanceof Resource) {
                return new SQLIntValue(value.asResource().getId());
            } else if(value instanceof ResourceIdentifierLiteral) {
                return new SQLIntValue(value.asIdentifier().getId());
            }
            throw new InvalidOperationException("Invalid argument in ResourceQuery: "
                    + value.getClass().getCanonicalName());

        case JSON_OBJ:
            // This should never happen... NEVER.
            logger.error("Trying to access the JSON-Column in Criteria!");
            throw new InvalidOperationException("Trying to access a JSON_OBJ as criteria!");

        default:
            break;
        }

        throw new InvalidOperationException("Trying to access a JSON_OBJ as criteria!");
    }

    /**
     * Finds the table for the specified resource type.
     * @param type
     * @return
     * @throws InvalidOperationException
     * @throws NoPathFoundException
     */
    private SQLTable findTable(String type) throws InvalidOperationException, NoPathFoundException {
        TableDefinition tdef = DatabaseDefinitions.get(type);
        SQLTable t = sqlQuery.findTable(tdef.getTable());
        if(t != null) {
            return t;
        }

        List<Path> paths = findPath(resultType, type, this.relations);

        if(paths.size() == 0) {
            throw new NoPathFoundException(resultType, type);
        }

        if(paths.size() > 1) {
            logger.error("Multiple path found from " + resultType + " to " + type + "!");
            for(Path path : paths) {
                logger.error("Path: " + resultType + " -> " + path.toString());
            }
            throw new InvalidOperationException("Multiple paths found from " + resultType + " to " + type);
        }

        for(PathEntry entry : paths.get(0).entries) {
            if(sqlQuery.findTable(entry.table) != null) {
                continue;
            }

            if(entry.join.joinType == JoinType.N_TO_M_JOIN) {
                TableDefinition from = DatabaseDefinitions.get(entry.origType);

                SQLTable tableFrom = sqlQuery.getTable(from.getTable());
                SQLTable tableTo = sqlQuery.getTable(entry.join.table);

                SQLColumn colFrom = tableFrom.getColumn(entry.origColumn, SQLColumnType.INTEGER);
                SQLColumn colTo = tableTo.getColumn(entry.joinColumn, SQLColumnType.INTEGER);

                sqlQuery.addJoin(tableTo, colFrom, colTo);

                Join other = findNtoMJoin(entry.join);

                TableDefinition tdefTo = DatabaseDefinitions.get(entry.resourceType);

                tableFrom = tableTo;
                tableTo = sqlQuery.getTable(tdefTo.getTable());

                colFrom = tableFrom.getColumn(other.joinedColumn, SQLColumnType.INTEGER);
                colTo = tableTo.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);

                sqlQuery.addJoin(tableTo, colFrom, colTo);

            } else {
                TableDefinition tdefFrom = DatabaseDefinitions.get(entry.origType);
                TableDefinition tdefTo = DatabaseDefinitions.get(entry.resourceType);

                if(sqlQuery.findTable(tdefFrom.getTable()) == null) {
                    throw new InvalidOperationException("Unknown Error!");
                }

                SQLTable tablefrom = sqlQuery.getTable(tdefFrom.getTable());
                SQLTable tableTo = sqlQuery.getTable(tdefTo.getTable());

                SQLColumn colFrom = tablefrom.getColumn(entry.origColumn, SQLColumnType.INTEGER);
                SQLColumn colTo = tableTo.getColumn(entry.joinColumn, SQLColumnType.INTEGER);

                sqlQuery.addJoin(tableTo, colFrom, colTo);
            }
        }
        TableDefinition def = DatabaseDefinitions.get(type);
        return sqlQuery.findTable(def.getTable());
    }

    public boolean isEmpty() {
        return criterias.size() == 0;
    }

    /**
     * Finds a path from resource type from to resource type to, using the array of relations for prioritized relations
     * @param from
     * @param to
     * @param relations prioritized relations
     * @return
     * @throws InvalidOperationException
     */
    static public List<Path> findPath(String from, String to, List<Relation> relations) throws InvalidOperationException {
        List<Path> paths = findPath(from, to, new ArrayDeque<String>(), relations);

        if(paths.size() > 1) {
            // If there are still multiple paths, select the shortest one
            logger.debug("Searching for shortest path!");
            int size = 0;
            Path p = null;

            for(Path path : paths) {
                if(size == 0 || path.entries.size() < size) {
                    size = path.entries.size();
                    p = path;
                }
            }
            logger.debug("returning shortest path: " + p.toString());
            paths = Arrays.asList(p);
        }

        return paths;
    }

    /**
     * Finds a Path from Entity type from to Entity type to. Uses the given list of prioritized relations
     * @param from
     * @param to
     * @param searched
     * @param relations
     * @return
     * @throws InvalidOperationException
     */
    static private List<Path> findPath(String from, String to,
            ArrayDeque<String> searched, List<Relation> relations) throws InvalidOperationException {
        if(searched.contains(from)) {
            return new ArrayList<Path>();
        }

        searched.push(from);
        List<Path> target = new ArrayList<Path>();
        List<PathEntry> relatedEntries = searchRelatedResources(from);

        for(PathEntry entry : relatedEntries) {
            if(entry.resourceType.equals(to)) {
                target.add(new Path(entry));
            } else {
                List<Path> paths = findPath(entry.resourceType, to, searched, relations);

                for(Path p : paths) {
                    p.add(entry);
                    target.add(p);
                }
            }
        }

        // If there are multiple paths, we need to find the ones prioritized in
        // the relations list
        if(target.size() > 1) {
            logger.debug("Multiple path found from " + from + " to " + to + ":");

            for(Path path : target) {
                logger.debug("Path: " + from + " -> " + path.toString());
            }

            List<Path> results = new ArrayList<Path>();
            Path result = null;

            // First we search the paths for entries, that are prioritized
            // and add those to our results list
            for(Path path : target) {
                for(PathEntry entry : path.entries) {
                    for(Relation relation : relations) {
                        if(((relation.resourceType.equals(entry.origType) && relation.resourceTypeTo.equals(entry.resourceType)) ||
                                (relation.resourceType.equals(entry.resourceType) && relation.resourceTypeTo.equals(entry.origType))) &&
                                (relation.property.equals(entry.join.inverseProperty) || relation.property.equals(entry.join.property))) {
                            result = path;
                            results.add(path);
                            logger.debug("Adding as prioritized path: " + path.toString());
                        }
                    }
                }
            }


            if(result != null) {
                // Once we found all prioritized paths, we select the shortest one

                logger.debug("Available paths: ");

                for(Path p : results) {
                    logger.debug("Path: " + p.toString());
                    if(p.entries.size() < result.entries.size()) {
                        result = p;
                    }
                }

                logger.debug("Selecting " + result.toString() + " as prioritized shortest path!");

                return Arrays.asList(result);
            }

            logger.debug("No predefined path found... continue...");
        }
        searched.pop();
        return target;
    }

    /**
     * Returns all Entities, that are directly related to the given Entity
     * @param type
     * @return
     */
    static private List<PathEntry> searchRelatedResources(String type) {
        List<PathEntry> target = new ArrayList<PathEntry>();

        for(String t : DatabaseDefinitions.getAll().keySet()) {
            if(t.equals(type)) {
                continue;
            }

            TableDefinition def = DatabaseDefinitions.get(t);
            for(Join join : def.getJoins()) {
                if(join.resourceType.equals(type) && join.joinType == JoinType.N_TO_1_JOIN) {
                    target.add(new PathEntry(join, def.getTable(), "id", join.joinedColumn, t, type));
                }
            }
        }

        for(Join join : DatabaseDefinitions.get(type).getJoins()) {
            target.add(new PathEntry(join ,join.table, join.joinedColumn, "id", join.resourceType, type));
        }

        return target;
    }

    /**
     * Finds the "reverse" n to m join.
     * @param join
     * @return
     */
    private static Join findNtoMJoin(Join join) {
        TableDefinition def = DatabaseDefinitions.get(join.resourceType);

        for(Join j : def.getJoins()) {
            if(j.table.equals(join.table)) {
                return j;
            }
        }

        return null;
    }

    /**
     * Thrown when there is no path from one resource to another.
     *
     */
    public static class NoPathFoundException extends DatabaseException {
        /**
         *
         */
        private static final long serialVersionUID = 3865921932005992695L;

        public NoPathFoundException(String from, String to) {
            super("No path found from " + from + " to " + to);
        }
    }

    /**
     * A Path entry
     *
     */
    static public class PathEntry {
        /**
         * The resource type
         */
        public final String resourceType;
        public final String origType;

        public final String joinColumn;
        public final String origColumn;
        public final String table;
        public final Join join;

        public PathEntry(Join join, String table, String jColumn, String origColumn, String resType,
                String origType) {
            this.join = join;
            this.table = table;
            this.joinColumn = jColumn;
            this.origColumn = origColumn;
            this.resourceType = resType;
            this.origType = origType;
        }

        @Override
        public String toString() {
            return resourceType + " (" + joinColumn + " = " + origColumn + ")";
        }
    }

    /**
     * A relation between two entities
     *
     */
    public static class Relation {
        public final String resourceType;
        public final String property;
        public final String resourceTypeTo;
        public final boolean nTo1;

        public Relation(String resourceType, String property, String resourceTypeTo, boolean nTo1) {
            this.resourceType = resourceType;
            this.property = property;
            this.resourceTypeTo = resourceTypeTo;
            this.nTo1 = nTo1;
        }
    }

    /**
     * A Path from Entity A to Entity B
     *
     */
    public static class Path {
        private final Deque<PathEntry> entries = new ArrayDeque<PathEntry>();

        public Path() {

        }

        public Deque<PathEntry> getEntries() {
            return entries;
        }

        public Path(PathEntry entry) {
            add(entry);
        }

        public boolean contains(String from) {
            for(PathEntry e : entries) {
                if(e.resourceType.equals(from)) {
                    return true;
                }
            }
            return false;
        }

        public void add(PathEntry entry) {
            entries.push(entry);
        }

        @Override
        public String toString() {
            return StringUtil.join(entries, " -> ", new Builder<PathEntry>() {
                @Override
                public String build(PathEntry o) {
                    return o.toString();
                }
            });
        }
    }

}
