/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The abstract list of criterias used for ANDs and ORs.
 *
 */
public abstract class ListCriteria extends Criteria {

    private final List<Criteria> criterias;

    /**
     * Creates a new list criteria of the same type as this and initializes it with the
     * given list of criterias.
     * @param criterias
     * @return
     */
    public abstract ListCriteria clone(Criteria... criterias);

    /**
     * Creates a new list criteria of the same type as this and initializes it with the
     * given list of criterias.
     * @param criterias
     * @return
     */
    public abstract ListCriteria clone(List<Criteria> criterias);

    protected ListCriteria(List<Criteria> cs) {
        criterias = new ArrayList<>();
        add(cs);
    }

    protected ListCriteria(Criteria... cs) {
        criterias = new ArrayList<>();
        add(cs);
    }

    public ListCriteria add(Criteria... criterias) {
        for(Criteria c : criterias) {
            this.criterias.add(c);
        }
        return this;
    }

    public ListCriteria add(List<Criteria> criterias) {
        this.criterias.addAll(criterias);
        return this;
    }

    public List<Criteria> getAll() {
        return Collections.unmodifiableList(criterias);
    }

}
