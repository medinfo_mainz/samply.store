/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.query;

import java.util.List;

/**
 * Creates a list of criterias that are linked together with a logical "OR".
 *
 */
public class OrCriteria extends ListCriteria {

    OrCriteria() {

    }

    /* (non-Javadoc)
     * @see de.samply.store.query.ListCriteria#clone(de.samply.store.query.Criteria[])
     */
    @Override
    public ListCriteria clone(Criteria... criterias) {
        return Criteria.Or(criterias);
    }

    /* (non-Javadoc)
     * @see de.samply.store.query.ListCriteria#clone(java.util.List)
     */
    @Override
    public ListCriteria clone(List<Criteria> criterias) {
        return Criteria.Or(criterias);
    }

}
