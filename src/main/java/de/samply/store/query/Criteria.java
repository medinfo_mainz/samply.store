/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.query;

import java.util.List;

import de.samply.store.BooleanLiteral;
import de.samply.store.NumberLiteral;
import de.samply.store.RangeLiteral;
import de.samply.store.StringLiteral;
import de.samply.store.Value;

/**
 * Helper class that creates new criterias. Use this class instead of creating new
 * instances with "new ..."
 *
 */
public abstract class Criteria {

    public static ValueCriteria Equal(String type, String property, Number value) {
        return new EqualCriteria(type, property, new NumberLiteral(value));
    }

    public static ValueCriteria Equal(String type, String property, String value) {
        return new EqualCriteria(type, property, new StringLiteral(value));
    }

    public static ValueCriteria Equal(String type, String property, Value value) {
        return new EqualCriteria(type, property, value);
    }

    public static ValueCriteria Equal(String type, String property, Boolean value) {
        return new EqualCriteria(type, property, new BooleanLiteral(value));
    }

    public static ValueCriteria Between(String type, String property, RangeLiteral range) {
        return new BetweenCriteria(type, property, range);
    }

    public static ValueCriteria Between(String type, String property, Number l, Number r) {
        return new BetweenCriteria(type, property, new RangeLiteral(l, r));
    }

    public static ValueCriteria Greater(String type, String property, Value value) {
        return new GreaterCriteria(type, property, value);
    }

    public static ValueCriteria Greater(String type, String property, Number value) {
        return new GreaterCriteria(type, property, new NumberLiteral(value));
    }

    public static ValueCriteria GreaterOrEqual(String type, String property, Value value) {
        return new GreaterOrEqualCriteria(type, property, value);
    }

    public static ValueCriteria GreaterOrEqual(String type, String property, Number value) {
        return new GreaterOrEqualCriteria(type, property, new NumberLiteral(value));
    }

    public static ValueCriteria Less(String type, String property, Value value) {
        return new LessCriteria(type, property, value);
    }

    public static ValueCriteria Less(String type, String property, Number value) {
        return new LessCriteria(type, property, new NumberLiteral(value));
    }

    public static ValueCriteria LessOrEqual(String type, String property, Value value) {
        return new LessOrEqualCriteria(type, property, value);
    }

    public static ValueCriteria LessOrEqual(String type, String property, Number value) {
        return new LessOrEqualCriteria(type, property, new NumberLiteral(value));
    }

    public static ValueCriteria NotEqual(String type, String property, Value value) {
        return new NotEqualCriteria(type, property, value);
    }

    public static ValueCriteria NotEqual(String type, String property, String value) {
        return new NotEqualCriteria(type, property, new StringLiteral(value));
    }

    public static ValueCriteria NotEqual(String type, String property, Number value) {
        return new NotEqualCriteria(type, property, new NumberLiteral(value));
    }

    public static ValueCriteria SimilarTo(String type, String property, String regex) {
        return new SimilarToCriteria(type, property, new StringLiteral(regex));
    }

    public static PropertyCriteria IsNull(String type, String property) {
        return new IsNullCriteria(type, property);
    }

    public static PropertyCriteria IsNotNull(String type, String property) {
        return new IsNotNullCriteria(type, property);
    }

    public static OrCriteria Or(Criteria... cs) {
        OrCriteria or = Or();
        return (OrCriteria) or.add(cs);
    }

    public static AndCriteria And(Criteria... cs) {
        AndCriteria and = And();
        return (AndCriteria) and.add(cs);
    }

    public static OrCriteria Or() {
        return new OrCriteria();
    }

    public static AndCriteria And() {
        return new AndCriteria();
    }

    /**
     * @param criterias
     * @return
     */
    public static ListCriteria Or(List<Criteria> criterias) {
        OrCriteria or = Or();
        or.add(criterias);
        return or;
    }

    /**
     * @param criterias
     * @return
     */
    public static ListCriteria And(List<Criteria> criterias) {
        AndCriteria and = And();
        and.add(criterias);
        return and;
    }

}
