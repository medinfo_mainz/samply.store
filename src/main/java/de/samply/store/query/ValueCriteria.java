/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.query;

import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLValue;
import de.samply.store.sql.clauses.Clause;

/**
 * An abstract class that describes a criteria for a specific resource
 * type, property and a specific value. E.g. for equality of the patients
 * property "name" to the value "Paul".
 *
 */
public abstract class ValueCriteria extends PropertyCriteria {

    public final Value value;

    ValueCriteria(String type, String property, Value value) {
        super(type, property);
        this.value = value;
    }

    @Override
    public Clause getClause(SQLColumn column) throws DatabaseException {
        throw new InvalidOperationException("Trying to access getClause(SQLColumn) in ValueCriteria!");
    }

    @Override
    public PropertyCriteria clone(String type, String property) {
        throw new UnsupportedOperationException("This is a ValueCriteria, calling clone with two parameters is not supported!");
    }

    public abstract ValueCriteria clone(String type, String property, Value value);

    public abstract Clause getClause(SQLValue l, SQLValue r);

}
