/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;
/**
 * The database constants: required column names, required tables (users, transactions)
 * and the database version code. This class is used by the PSQLModel.
 *
 */
public class DatabaseConstants {

    /**
     * Code for PostgreSQL version:
     * 1 - 9.3
     * 2 - 9.4
     */
    public static int PSQLVersionCode = 2;

    public final static String idColumn = "id";
    public final static String oldIdColumn = "oid";
    public final static String dataColumn = "data";
    public final static String transactionsIdColumn = "transaction_id";
    public final static String historyPostfix = "_history";
    public final static String timeColumn = "timestamp";

    public final static String usersTable = "users";
    public final static String transactionsTable = "transactions";

    public final static String usernameColumn = "username";
    public final static String failCounterColumn = "failCounter";
    public final static String isActivatedColumn = "isActivated";

    public static String getSQLJson() {
        if(PSQLVersionCode >= 2) {
            return "JSONB";
        } else {
            return "JSON";
        }
    }

}
