/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.sql;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Bins a list of values to a prepared statement.
 *
 */
public class Binder {

    /**
     * The SQLValues that will be bound to the prepared statement.
     */
    private List<SQLValue> values = new ArrayList<>();

    /**
     * Adds another variable the this binder.
     * @param value
     */
    public void addVariable(SQLValue value) {
        values.add(value);
    }

    /**
     * Binds the values to the specified prepared statement.
     * @param stmt
     * @throws SQLException
     */
    public void bind(PreparedStatement stmt) throws SQLException {
        int index = 0;
        for(int i = 0; i < values.size(); ++i, ++index) {
            SQLValue v = values.get(i);
            if(v instanceof SQLStringValue) {
                stmt.setString(index + 1, ((SQLStringValue) v).getValue());
            } else if(v instanceof SQLIntValue) {
                stmt.setInt(index + 1, ((SQLIntValue) v).getValue());
            } else if(v instanceof SQLFloatValue) {
                stmt.setFloat(index + 1, ((SQLFloatValue) v).getValue());
            } else if(v instanceof SQLLongValue) {
                stmt.setLong(index + 1, ((SQLLongValue) v).getValue());
            } else if(v instanceof SQLTimestampValue) {
                stmt.setTimestamp(index + 1, ((SQLTimestampValue) v).getValue());
            } else if(v instanceof SQLRangeValue) {
                stmt.setObject(index + 1, ((SQLRangeValue) v).getLeft());
                stmt.setObject(index + 2, ((SQLRangeValue) v).getRight());
                ++index;
            } else if(v instanceof SQLBooleanValue) {
                stmt.setBoolean(index + 1, ((SQLBooleanValue) v).getValue());
            }
        }
    }

}
