/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.sql.functions;

import de.samply.store.sql.Binder;
import de.samply.store.sql.SQLColumn;

/**
 * Returns the type of the resource identifier using the resourceType SQL function.
 *
 */
public class SQLResourceType extends SQLFunction {

    private final SQLColumn column;

    /**
     * Creates a SQL function that returns the resource type of the given SQL column.
     * This requires the SQL column to be a JSON column.
     * @param column
     */
    public SQLResourceType(SQLColumn column) {
        this.column = column;
    }

    @Override
    public String construct(Binder b) {
        return "resourceType(" + this.column.construct(b) + ")";
    }
}
