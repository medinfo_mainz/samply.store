/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.sql;

/**
 * Represents a SQL column from a table. It uses a generated alias.
 *
 */
public class SQLColumn implements SQLValue {

    /**
     * The table column name
     */
    public final String column;

    /**
     * The column alias.
     */
    public final String alias;

    /**
     * The SQL column type
     */
    public final SQLColumnType type;

    /**
     * The json property of the data column if necessary, otherwise null.
     */
    public final String jsonProperty;

    /**
     * An abstract constant. If this value is not null, other fields are null: column and json property.
     */
    public final AbstractSQLValue<?> constant;

    /**
     * The SQL table for this SQL column.
     */
    public final SQLTable table;

    /**
     * Copies the given SQL column.
     * @param table
     * @param old
     */
    public SQLColumn(SQLTable table, SQLColumn old) {
        this.column = old.column;
        this.alias = old.alias;
        this.type = old.type;
        this.jsonProperty = null;
        this.constant = null;

        this.table = table;
    }

    /**
     * Creates a new constant value as a SQL column.
     * @param t
     * @param constant
     * @param type
     * @param alias
     */
    public SQLColumn(SQLTable t, AbstractSQLValue<?> constant, SQLColumnType type, String alias) {
        this.table = t;
        this.column = null;
        this.type = type;
        this.alias = alias;
        this.jsonProperty = null;
        this.constant = constant;
    }

    /**
     * Constructs a new SQL column with the given attributes.
     * @param table
     * @param column
     * @param alias
     * @param type
     */
    public SQLColumn(SQLTable table, String column, String alias, SQLColumnType type) {
        this.column = column;
        this.table = table;
        this.type = type;
        this.alias = alias;
        this.jsonProperty = null;
        this.constant = null;
    }

    /**
     * Constructs a new SQL column that accesses the given property from the given JSON column.
     * @param table
     * @param column
     * @param alias
     * @param type
     * @param jsonProperty
     */
    public SQLColumn(SQLTable table, String column, String alias, SQLColumnType type,
            String jsonProperty) {
        this.column = column;
        this.table = table;
        this.type = type;
        this.alias = alias;
        this.jsonProperty = jsonProperty;
        this.constant = null;
    }

    /**
     * Creates the SQL string that can be used to access the column, e.g. t_0."data"#>>'{name}'.
     * @param b
     * @return
     */
    public String access(Binder b, boolean arrayColumn) {
        if(this.constant != null) {
            return this.constant.construct(b);
        } else {
            String tableCol = table.getAlias() + ".\"" + column + "\"";
            if(jsonProperty != null) {
                String property = jsonProperty;
                if(arrayColumn) {
                    property = jsonProperty + ",column";
                }

                switch(type) {
                    case JSON_STRING:
                        return "(" + tableCol + "#>>'{" + property + "}')::text";

                    case JSON_FLOAT:
                        return "(" + tableCol + "#>>'{" + property + "}')::float";

                    case JSON_INT: // To be on the safe route
                    case JSON_LONG:
                        return "(" + tableCol + "#>>'{" + property + "}')::bigint";

                    case JSON_TIMESTAMP:
                        return "(" + tableCol + "#>>'{" + property + "}')::timestamp";

                    case JSON_BOOLEAN:
                        return "(" + tableCol + "#>>'{" + property + "}')::boolean";

                    case JSON_OBJ: // Treat it as a JSON object
                        return "(" + tableCol + "#>'{" + property + "}')::json";

                    default:
                        break;
                }
            }
            return tableCol;
        }
    }

    public String access(Binder b) {
        return access(b, false);
    }

    @Override
    public String construct(Binder b) {
        return access(b);
    }
}
