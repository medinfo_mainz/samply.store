/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.samply.store.TableDefinition.Join;

/**
 * All TableDefinitions are dynamically added here. Uses a static HashMap
 * to store those TableDefinitions.
 *
 */
public class DatabaseDefinitions {

    private static final HashMap<String, TableDefinition> definitions = new HashMap<>();

    /**
     * Removes all table definitions.
     */
    public static void clear() {
        definitions.clear();
    }

    /**
     * Adds a table definition.
     * @param type
     * @param def
     */
    public static void put(String type, TableDefinition def) {
        definitions.put(type, def);
    }

    /**
     * Gets the table definition for the specified type.
     * @param type
     * @return
     */
    public static TableDefinition get(String type) {
        return definitions.get(type);
    }

    public static Map<String, TableDefinition> getAll() {
        return Collections.unmodifiableMap(definitions);
    }

    /**
     * Returns a list of Strings of entity types of parents
     * for the specified type.
     * @param type
     * @return the list of parents. May be empty.
     */
    public static List<String> getParent(String type) {
        List<String> target = new ArrayList<>();

        for(TableDefinition tdef : getAll().values()) {
            for(Join join : tdef.getJoins()) {
                if(join.resourceType.equals(type)) {
                    target.add(tdef.resourceType);
                }
            }
        }

        return target;
    }

    /**
     * Get all children of the specified type.
     * @param type
     * @return A HashMap where the keys represent properties and the values the respective entity types.
     * Returns an empty map if no children exist.
     */
    public static Map<String, String> getChildren(String type) {
        Map<String, String> target = new HashMap<>();
        TableDefinition tdef = get(type);

        for(Join join : tdef.getJoins()) {
            target.put(join.property, join.resourceType);
        }

        return target;
    }
}
