/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Some utils used for resources.
 *
 */
public class ResourceUtils {

    /**
     * A Diff Resource represents the difference (like the diff command on UNIX machines)
     * between two resources.
     *
     */
    public static class DiffResource {

        public final JSONResource plus;
        public final JSONResource minus;

        DiffResource(JSONResource plus, JSONResource minus) {
            this.plus = plus;
            this.minus = minus;
        }
    }

    /**
     * Creates a diff-resource for the specified resources.
     *
     * @param From
     * @param To
     * @return
     */
    public static DiffResource createDiffResource(AbstractResource From, AbstractResource To) {
        if (From == null)
            throw new NullPointerException("Argument From is null!");
        if (To == null)
            throw new NullPointerException("Argument To is null!");

        JSONResource plus = new JSONResource();
        JSONResource minus = new JSONResource();

        for(String p : From.getDefinedProperties()) {
            for(Value f : From.getProperties(p)) {
                if(!(f instanceof Literal<?>)) {
                    continue;
                }

                boolean found = false;
                ArrayList<Value> tvalues = To.getProperties(p);
                if(tvalues != null) {

                    for(Value t : tvalues) {
                        if(f.equals(t)) {
                            found = true;
                        }
                    }
                }
                if(!found) {
                    minus.addProperty(p, f);
                }
            }
        }

        for(String p : To.getDefinedProperties()) {
            for(Value f : To.getProperties(p)) {
                if(!(f instanceof Literal<?>)) {
                    continue;
                }

                boolean found = false;
                ArrayList<Value> tvalues = From.getProperties(p);
                if(tvalues != null) {

                    for(Value t : tvalues) {
                        if(f.equals(t)) {
                            found = true;
                        }
                    }
                }
                if(!found) {
                    plus.addProperty(p, f);
                }
            }
        }


        return new DiffResource(plus, minus);
    }

    /**
     * Creates a GSON JsonObject using the given resources values HashMap
     * @param values the resources values HashMap (use get() to get it from the resource, package visibility)
     * @return the GSON JsonObject using the createJsonElement mapper.
     */
    public static JsonObject createJsonObject(HashMap<String, ArrayList<Value>> values) {
        JsonObject data = new JsonObject();
        for(String prop : values.keySet()) {
            if(values.get(prop).size() == 1) {
                Value value = values.get(prop).get(0);
                data.add(prop, createJsonElement(value));
            } else {
                JsonArray array = new JsonArray();
                for(Value value : values.get(prop)) {
                    array.add(createJsonElement(value));
                }
                data.add(prop, array);
            }
        }
        return data;
    }

    /**
     * Creates a GSON JsonElement from the given Value. This is a simple mapping method that maps
     * values (samply store Value) to GSON objects. This is used to generate the JSON string for
     * PostgreSQL.
     * @param value the samply store Value to map
     * @return a GSON JsonPrimitive where possible (Literals etc.), otherwise a JsonObject
     */
    public static JsonElement createJsonElement(Value value) {
        if(value instanceof NumberLiteral) {
            return new JsonPrimitive(((NumberLiteral) value).get());
        } else if(value instanceof TimestampLiteral) {
            return new JsonPrimitive(((TimestampLiteral) value).get().toString());
        } else if(value instanceof JSONResource) {
            HashMap<String, ArrayList<Value>> values = ((AbstractResource) value).get();
            return createJsonObject(values);
        } else if(value instanceof BooleanLiteral) {
            return new JsonPrimitive(value.asBoolean());
        } else if(value instanceof Resource) {
            return new JsonPrimitive(value.asResource().getIdentifier().toString());
        } else {
            return new JsonPrimitive(value.getValue());
        }
    }

}
