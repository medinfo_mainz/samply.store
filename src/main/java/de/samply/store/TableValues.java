/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import de.samply.store.sql.SQLColumnType;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * Used in PSQLModel to insert rows into tables and update existing rows in tables.
 */
public class TableValues {

    /**
     * The current column index. Used for statement.setObject(index ...)
     */
    private int index = 1;

    /**
     * All available fields
     */
    private ArrayList<TableValue> fields = new ArrayList<>();

    /**
     * Creates a new value for the given column name and column type.
     * @param name the column name
     * @param value the value
     * @param type the column type
     * @return
     */
    public TableValue createField(String name, Object value, SQLColumnType type) {
        TableValue field = new TableValue();
        field.index = index++;
        field.name = name;
        field.value = value;
        field.type = type;

        fields.add(field);
        return field;
    }

    /**
     * Binds the values to the given prepared statement.
     * @param stmt
     * @throws SQLException
     */
    public void bindValues(PreparedStatement stmt) throws SQLException {
        for(TableValue field : fields) {
            stmt.setObject(field.index, field.value);
        }
    }

    /**
     * Returns the columns as joined string, e.g. '"col1", "col2"'
     * @return
     */
    public String getInsertColumns() {
        return StringUtil.join(fields, ",", new Builder<TableValue>() {
            @Override
            public String build(TableValue o) {
                return "\"" + o.name + "\"";
            }
        });
    }

    /**
     * Creates a string that can be used to insert new values into a table.
     * @return
     */
    public String getInsertBindValues() {
        return StringUtil.join(fields, ",", new Builder<TableValue>() {
            @Override
            public String build(TableValue o) {
                if(o.type == SQLColumnType.JSON_OBJ) {
                    return "CAST(? AS " + DatabaseConstants.getSQLJson() + ")";
                } else {
                    return "?";
                }
            }
        });
    }

    /**
     * Creates a string of table values that can be used to execute a SQL UPDATE statement.
     * @return
     */
    public String getUpdateBindValues() {
        return StringUtil.join(fields, ",", new Builder<TableValue>() {
            @Override
            public String build(TableValue o) {
                if(o.type == SQLColumnType.JSON_OBJ) {
                    return "\"" + o.name + "\" = CAST(? AS " + DatabaseConstants.getSQLJson() + ")";
                } else {
                    return "\"" + o.name + "\" = ?";
                }
            }
        });
    }

    /**
     * A value that will be inserted into a table.
     *
     */
    private class TableValue {

        public int index;
        public String name;
        public Object value;
        public SQLColumnType type;

    }
}
