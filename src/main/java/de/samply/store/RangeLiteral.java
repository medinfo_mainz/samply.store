/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import de.samply.store.sql.SQLColumnType;

/**
 * Represents a range (a <= b <= x). Used in ResourceQuery for ranges.
 *
 */
public class RangeLiteral extends Literal<Number> {

    private static final long serialVersionUID = -1331315340180133844L;
    public final Number from;
    public final Number to;

    /**
     * Constructs an instance with the given limits.
     * @param from Lower bound (inclusive)
     * @param to Upper bound (inclusive)
     */
    public RangeLiteral(Number from, Number to) {
        super(from);

        if(!from.getClass().equals(to.getClass())) {
            throw new RuntimeException("RangeLiteral needs two Integers, two Floats or two Longs!");
        }

        this.from = from;
        this.to = to;
    }

    @Override
    public String getValue() {
        return from + "-" + to;
    }

    /**
     * Gets the lower bound of the represented range.
     */
    public Number getFrom() {
        return from;
    }

    /**
     * Gets the upper bound of the represented range.
     */
    public Number getTo() {
        return to;
    }

    @Override
    public String toString() {
        return from + "-" + to + "::range";
    }

    @Override
    public SQLColumnType getCorrespondingSQLType() {
        if(from instanceof Integer) {
            return SQLColumnType.JSON_INT;
        } else if(from instanceof Float) {
            return SQLColumnType.JSON_FLOAT;
        } else {
            return SQLColumnType.JSON_LONG;
        }
    }

}