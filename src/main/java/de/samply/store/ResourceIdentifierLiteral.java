/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import de.samply.store.sql.SQLColumnType;

/**
 * A resource identifier literal identifies a resource. It uses
 * a string and an integer to do so. The actual string representation is
 * "type:id::resourceIdentifier".
 *
 */
public class ResourceIdentifierLiteral extends Literal<String> implements Identifiable {

    /**
     *
     */
    private static final long serialVersionUID = 3108158716502304176L;
    private final String type;
    private final int id;

    public ResourceIdentifierLiteral(String type, int id) {
        super(type + ":" + id);
        this.type = type;
        this.id = id;
    }

    @Override
    public String getValue() {
        return type + ":" + id;
    }

    @Override
    public String toString() {
        return getValue() + "::resourceIdentifier";
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public SQLColumnType getCorrespondingSQLType() {
        return SQLColumnType.JSON_STRING;
    }

    /**
     * Parse an instance from a string. The format of the string must be: "$type:$id::resourceIdentifier"
     * @param input
     * @return
     */
    public static ResourceIdentifierLiteral fromString(String input) {
        String[] split = input.split(":");
        if(split.length == 4 && split[2].equals("") && split[3].equals("resourceIdentifier")) {
            return new ResourceIdentifierLiteral(split[0], Integer.parseInt(split[1]));
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
}
