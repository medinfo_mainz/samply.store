/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.store.PSQLModel.LoginInformation;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.exceptions.NotAuthorizedException;
import de.samply.store.query.ResourceQuery;

/**
 * The AccessController modifies SQLQuerys
 * in order to control the access to resources.
 *
 * @param <T> The context object used for every request. This class is only relevant for your
 *  AccessController but not for the PSQLModel.
 */
public class AccessController<T> {

    /**
     * The current login. Null is there was no login yet.
     */
    private LoginInformation login;

    /**
     * The DatabaseModel on which this controller operates.
     */
    protected DatabaseModel<T> model;

    /**
     * Is called after a role has been selected by the user
     * @param role
     * @throws DatabaseException
     */
    public void roleSelected(Resource role) throws DatabaseException {

    }

    /**
     * Is called after a successful login
     * @param login
     */
    public void login(LoginInformation login) {
        this.login = login;
    }

    /**
     * Called to check if the user has the permission to create
     * the given resource
     * @param resource
     * @throws NotAuthorizedException
     * @throws DatabaseException
     */
    public boolean canCreateResource(Resource resource) throws NotAuthorizedException, DatabaseException {
        return true;
    }

    /**
     * Called to check if the user has the permission to edit
     * the given resource
     * @param resource
     * @throws DatabaseException
     */
    public boolean canEditResource(Resource resource) throws DatabaseException {
        return true;
    }

    /**
     * Called before the query is built and executed. Used to get resources.
     * @param context
     * @param type
     * @throws DatabaseException
     */
    public void configureGet(AccessContext<T> context, String type) throws DatabaseException {

    }

    /**
     * Checks if the user is permitted to access the history of the given resource.
     * @param resource
     */
    public boolean canAccessHistory(Resource resource) {
        return true;
    }

    /**
     * Called after the query has been executed and the resources have been built.
     * Use this function to remove properties from the resource if necessary.
     * @param resource the actual resource whose properties must be filtered
     * @param context the access controller context
     * @param resourceType the resource type
     */
    public void filterProperties(Resource resource, AccessContext<T> context, String resourceType) {

    }

    /**
     * Called *once* for every resource SQL row
     * @param resource the actual resource
     * @param context the access controller context
     * @param set the SQL result set
     * @throws SQLException
     */
    public void dataRow(Resource resource, AccessContext<T> context, ResultSet set) throws SQLException {

    }

    /**
     * Called after the query has been executed and the resources have been built.
     * Use this function to remove properties from the resource if necessary.
     * @param resources the resources whose properties must be filtered
     * @param context the access controller context
     * @param resourceType the resource type
     */
    public void filterProperties(List<Resource> resources, AccessContext<T> context, String resourceType) {
        for(Resource r : resources) {
            filterProperties(r, context, resourceType);
        }
    }

    /**
     * Sets the reference to the DatabaseModel used for this AccessController
     * @param model
     */
    protected void setModel(DatabaseModel<T> model) {
        this.model = model;
    }

    /**
     * Returns the result resources of the specified resource query. If internal is true,
     * the model does not call the access controller.
     * @param query
     * @param internal
     * @return the list of resources (may be empty)
     * @throws DatabaseException
     */
    protected List<Resource> getResources(ResourceQuery query, boolean internal) throws DatabaseException {
        return this.model.getResources(query, internal);
    }

    /**
     * Called after the PSQLModel.logout method has been called.
     */
    public void logout() {

    }

    /**
     * Returns the current user login
     */
    public LoginInformation getLogin() {
        return this.login;
    }

    /**
     * Checks if the current user is permitted to get the configuration "name"
     * @param name
     * @throws InvalidOperationException
     */
    public boolean canGetConfig(String name) throws InvalidOperationException {
        return true;
    }

    /**
     * Checks if the current user is permitted to save the configuration
     * "config" as "name"
     * @param name
     * @param config
     * @throws InvalidOperationException
     */
    public boolean canSaveConfig(String name, JSONResource config) throws InvalidOperationException {
        return true;
    }

    /**
     * Checks if the resource can be deleted by the current user
     * @param resource
     * @throws DatabaseException
     */
    public boolean canDeleteResource(Resource resource) throws DatabaseException {
        return false;
    }

    /**
     * Checks if the current user is authorized to execute the specified action with the
     * specified parameters.
     * @param name
     * @param parameters
     * @throws DatabaseException
     */
    public boolean canExecuteAction(String name, Object... parameters) throws DatabaseException {
        return true;
    }

    /**
     * Executes the action specified by the name with the specified parameters. If you want to see all
     * possible actions, refer to the implementations documentation (e.g. Samply.Store.OSSE)
     * @param name
     * @param parameters
     * @throws DatabaseException
     */
    public void executeAction(String name, Object... parameters) throws DatabaseException {
    }

    /**
     * Saves the specified resources, ignores the access controller.
     * @param resources
     * @throws DatabaseException
     */
    protected void saveResourcesInternal(Resource... resources) throws DatabaseException {
        model.saveOrUpdateResourcesInternal(resources);
    }

    /**
     * Prepares the given resource for saving (update or create), remove unnecessary properties in
     * this method.
     * @param resource
     */
    public void prepareSaveResource(Resource resource) {
    }

}
