--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Contact: info@osse-register.de
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

BEGIN TRANSACTION;

ALTER TABLE transactions DROP CONSTRAINT transactions_user_id_fkey;

INSERT INTO transactions ("timestamp", user_id) values (now(), 1);
INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'admin', '6bcf0beed43ffcbb06d778c5e00dab81f901c71162bb1ac06ceb51309829e7466219d7a6af315a6583668ddd7016e9bf72bbed1e6ba269d22d8e1c17c8c52acd', 'gyQq5bsnpfE5RkeN5Z3iTknLQqt40VsL', 1, '', 0, 1);

INSERT INTO locations (transaction_id, name, data) VALUES (1, 'Globale Rollen', '{}');
INSERT INTO roles (data, name, transaction_id, location_id) values ('{"roleType":"ADMINISTRATOR", "isUserRole":false}', 'Globale Admins', 1, 1);
INSERT INTO users_roles (transaction_id, user_id, role_id) VALUES (1, 1, 1);

ALTER TABLE transactions ADD FOREIGN KEY (user_id) REFERENCES users (id);

COMMIT;