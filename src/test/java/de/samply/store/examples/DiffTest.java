/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.examples;

import de.samply.store.AbstractResource;
import de.samply.store.JSONResource;
import de.samply.store.ResourceUtils;
import de.samply.store.Value;
import de.samply.store.ResourceUtils.DiffResource;

public class DiffTest {

    public static void main(String[] args) {
        JSONResource p1 = new JSONResource();
        JSONResource p2 = new JSONResource();

        p1.addProperty("age", 20);
        p2.addProperty("age", 21.2039123f);

        p1.addProperty("first_name", "Paul");
        p2.addProperty("name", "Paulinator");

        DiffResource diff = ResourceUtils.createDiffResource(p1, p2);

        System.out.println("Plus:");
        printResource(diff.plus);

        System.out.println("\n\nMinus:");
        printResource(diff.minus);

    }


    /**
     * Gibt die Ressource auf die Konsole aus
     * @param resource
     */
    public static void printResource(AbstractResource resource) {
        System.out.println("Resource: " + resource.getValue());
        for(String s : resource.getDefinedProperties()) {
            if(resource.getProperties(s).size() > 1) {
                System.out.println("Property: " + s);
                for(Value v : resource.getProperties(s)) {
                    System.out.println("\t" + v);
                }
            } else {
                System.out.println(s + " : " + resource.getProperty(s));
            }
        }
    }

}
