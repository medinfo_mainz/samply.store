/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.examples;

import java.sql.SQLException;

import de.samply.store.AccessController;
import de.samply.store.DatabaseModel;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.history.Transaction;
import de.samply.store.test.TestOntology;

/**
 * Dieses Beispiel zeigt wie man:
 * - eine Ressource aus der Datenbank holt
 * - neue Ressourcen erstellt und sie miteinander verknüpft
 * - neue Ressourcen abspeichert
 * - die History einer Ressource abruft
 *
 *
 */
public class Main {


    public static void main(String[] args) throws SQLException, DatabaseException {
        /**
         * Das Model wird initialisiert, der Template-Parameter gibt die Klasse an, die der
         * eigene AccessController nutzt. Erstmal ist die nicht weiter wichtig.
         *
         * Wichtig: Die Zugangsdaten müssen in der XML-Config backend.xml richtig konfiguriert
         * werden!
         */
        DatabaseModel<Void> model = new PSQLModel<Void>(new AccessController<Void>(), "unit.tests.database.xml");

        /**
         * Man muss sich einloggen, ansonsten werden immer Exceptions geworfen.
         *
         * Der Benutzer "neverever" mit dem Passwort "admin" wird bei einer richtigen Installation
         * entfernt und durch einen vom Client erzeugten Benutzer ersetzt
         */
        model.login("neverever", "admin");

        /**
         * Es wird die Location mit der ID 1 aus der Datenbank geholt
         */
        Resource location = model.getResource(TestOntology.Type.Location, 1);

        /**
         * Es wird ein neuer Patient angelegt. Er ist noch nicht in der Datenbank gespeichert!
         */
        Resource patient = model.createResource(TestOntology.Type.Patient);

        /**
         * Der Patienten wird mit der Location "location" verknüpft und
         * dem Patienten werden einige Properties zugewiesen.
         */
        patient.setProperty("first_name", "Stefan");
        patient.setProperty("last_name", "Muster");

        /**
         * Wir speichern die neue Ressource
         */
        commitResource(model, patient);

        /**
         * Der Patient wird verändert: es werden neue Properties zugewiesen
         */
        patient.setProperty("age", 23);
        patient.setProperty("born_year", 1990);

        /**
         * Erneut speichern
         */
        commitResource(model, patient);

        /**
         * Es wird ein Fall erstellt, dem Patienten zugewiesen
         * und in der DB gespeichert
         */
        Resource pCase = model.createResource(TestOntology.Type.Case);
        pCase.setProperty(TestOntology.Case.Patient, patient);
        pCase.setProperty(TestOntology.Case.Location, location);
        pCase.setProperty("blood_pressure", 90.2f);
        pCase.setProperty("blood_sugar", 102.5f);

        commitResource(model, pCase);

        /**
         * Der Patient wird erneut verändert
         */
        patient.setProperty("cancer_found", "yes");
        commitResource(model, patient);

        /**
         * Und dem Patienten wird noch ein Fall zugewiesen
         */
        pCase = model.createResource(TestOntology.Type.Case);
        pCase.setProperty(TestOntology.Case.Patient, patient);
        pCase.setProperty("cancer_type", "A");
        pCase.setProperty("cancer_location", "brain");
        commitResource(model, pCase);


        /**
         * Anschließend wird der Patient nochmal aus der Datenbank geladen,
         * damit er wirklich aktuell ist.
         */
        System.out.println("Aktueller Stand:");
        patient = model.getResource(TestOntology.Type.Patient, patient.getId());
        printResource(patient);

        /**
         * Jetzt wird durch die einzelnen Transaktionen des Patienten
         * iteriert und die Ressource auf der Konsole ausgegeben.
         *
         * Das Ergebnis:
         *
         * Die Versionen des Patienten werden in chronologischer Reihenfolge ausgegeben:
         * Zuerst der aktuelle Stand des Patienten (einmal wie er aktuell in der DB gespeichert ist und einmal wie er
         * zum Zeitpunkt der Transaktion, mit der er in der DB gespeichert wurde).
         * Der Unterschied:
         * Aktuell besitzt der Patient zwei Fälle, aber zum Zeitpunkt der Transaktion besaß er nur einen Fall.
         *
         * Anschließend kommen die älteren Versionen des Patienten.
         *
         */
        for(Transaction t : model.getTransactions(patient)) {
            Resource old = model.getResource(patient, t);
            System.out.println("-------");
            System.out.println("Zeitpunkt: " + t.time);
            printResource(old);
            System.out.println("-------");
        }
    }

    /**
     * Damit eine Ressource gespeichert werden kann, muss zuerst:
     * - eine Tranksaktion erstellt werden
     * - saveOrUpdateResource (auch mehrmals) aufgerufen werden
     * - der Commit durchgeführt werden
     * @param model
     * @param resource
     * @throws DatabaseException
     */
    private static void commitResource(DatabaseModel<?> model, Resource resource) throws DatabaseException {
        model.beginTransaction();
        model.saveOrUpdateResource(resource);
        model.commit();
    }

    /**
     * Gibt die Ressource auf die Konsole aus
     * @param resource
     */
    public static void printResource(Resource resource) {
        System.out.println("Resource: " + resource.getValue());
        for(String s : resource.getDefinedProperties()) {
            if(resource.getProperties(s).size() > 1) {
                System.out.println("Property: " + s);
                for(Value v : resource.getProperties(s)) {
                    System.out.println("\t" + v);
                }
            } else {
                System.out.println(s + " : " + resource.getProperty(s));
            }
        }
    }
}