/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.test.subtests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.samply.store.BooleanLiteral;
import de.samply.store.NumberLiteral;
import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.StringLiteral;
import de.samply.store.TimestampLiteral;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.test.TestOntology;

public class TypeDatabaseTest extends AbstractDatabaseTest {

    @Test
    public void datatypeTest() throws DatabaseException {
        Resource group = getLocation();
        Resource patient = model.createResource(TestOntology.Type.Patient);
        Resource pcase = model.createResource(TestOntology.Type.Case);
        Resource p2 = model.createResource(TestOntology.Type.Patient);

        p2.setProperty("name", "Paulinator");

        pcase.setProperty(TestOntology.Case.Patient, patient);
        pcase.setProperty(TestOntology.Case.Location, group);

        patient.setProperty("integer", 10);
        patient.setProperty("float", 10.5f);
        patient.setProperty("long", 1238934589239234L);
        patient.setProperty("string", "TEST");
        patient.setProperty("timestamp", new TimestampLiteral(5));
        patient.setProperty("true", new BooleanLiteral(true));
        patient.setProperty("false", new BooleanLiteral(false));
        patient.setProperty("father", p2);
        patient.setProperty("date", "2010-02-01T07:35:22Z");


        assertTrue(patient.getProperty("integer") instanceof NumberLiteral);
        assertTrue(patient.getProperty("float") instanceof NumberLiteral);
        assertTrue(patient.getProperty("long") instanceof NumberLiteral);
        assertTrue(patient.getProperty("string") instanceof StringLiteral);
        assertTrue(patient.getProperty("timestamp") instanceof TimestampLiteral);
        assertTrue(patient.getProperty("false") instanceof BooleanLiteral);
        assertTrue(patient.getProperty("father") instanceof Resource);
        assertTrue(patient.getProperty("date") instanceof StringLiteral);

        saveResources(p2, patient, pcase);

        patient = model.reloadResource(patient);
        assertTrue(patient.getProperty("integer") instanceof NumberLiteral);
        assertTrue(patient.getProperty("float") instanceof NumberLiteral);
        assertTrue(patient.getProperty("long") instanceof NumberLiteral);
        assertTrue(patient.getProperty("timestamp") instanceof TimestampLiteral);
        assertTrue(patient.getProperty("string") instanceof StringLiteral);
        assertTrue(patient.getProperty(TestOntology.Patient.ReadOnly.Cases) instanceof ResourceIdentifierLiteral);
        assertTrue(patient.getProperty("true") instanceof BooleanLiteral);
        assertTrue(patient.getProperty("false") instanceof BooleanLiteral);
        assertTrue(patient.getProperty("father") instanceof ResourceIdentifierLiteral);
        assertTrue(patient.getProperty("date") instanceof StringLiteral);

        assertTrue(patient.getProperty("integer").asInteger() == 10);
        assertTrue(patient.getProperty("float").asFloat() == 10.5f);
        assertTrue(patient.getProperty("long").asLong() == 1238934589239234L);
        assertTrue(patient.getProperty("string").getValue().equals("TEST"));
        assertTrue(patient.getProperty("timestamp").asTimestamp().getTime() == 5);
        assertTrue(patient.getProperty("true").asBoolean());
        assertFalse(patient.getProperty("false").asBoolean());
    }
}
