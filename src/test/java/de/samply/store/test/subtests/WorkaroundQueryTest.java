/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Random;

import org.junit.Test;

import de.samply.store.RangeLiteral;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import de.samply.store.test.TestOntology;


public class WorkaroundQueryTest extends AbstractDatabaseTest {

    @Test
    public void addPatientAndQuery() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);

        Resource Case = model.createResource(TestOntology.Type.Case);
        Case.setProperty(TestOntology.Case.Patient, patient);
        Case.setProperty(TestOntology.Case.Location, getLocation());

        Resource sample = model.createResource(TestOntology.Type.Sample);
        sample.setProperty(TestOntology.Sample.Case, Case);

        int sProp = new Random().nextInt(144444444);
        int cProp = new Random().nextInt(144444444);
        String pName = "nothing_to_see_" + new Random().nextInt();

        sample.setProperty("random-number", sProp);
        Case.setProperty("another", cProp);
        patient.setProperty("name", pName);
        patient.setProperty("age", 5);

        saveResources(patient, Case, sample);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.addRelation(TestOntology.Type.Case, TestOntology.Case.Patient, TestOntology.Type.Patient);
        query.addRelation(TestOntology.Type.Case, TestOntology.Case.Location, TestOntology.Type.Location, true);

        query.addEqual("name", pName);
        query.add(Criteria.Between(TestOntology.Type.Patient, "age", new RangeLiteral(0, 6)));
        query.add(Criteria.Equal(TestOntology.Type.Location, TestOntology.ID, getLocation().getId()));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "random-number", sProp));

        // Test the getPatients method
        List<Resource> results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == patient.getId());


        query = new ResourceQuery(TestOntology.Type.Case);
        query.add(Criteria.Equal(TestOntology.Type.Location, TestOntology.ID, getLocation().getId()));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "random-number", sProp));
        query.add(Criteria.Equal(TestOntology.Type.Case, "another", cProp));

        // Test the getCases method
        results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == Case.getId());


        query = new ResourceQuery(TestOntology.Type.Sample);
        query.add(Criteria.Equal(TestOntology.Type.Patient, "name", pName));
        query.add(Criteria.Between(TestOntology.Type.Patient, "age", new RangeLiteral(0, 6)));
        query.add(Criteria.Equal(TestOntology.Type.Location, TestOntology.ID, getLocation().getId()));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "random-number", sProp));
        query.add(Criteria.Equal(TestOntology.Type.Case, "another", cProp));

        results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == sample.getId());
    }

}
