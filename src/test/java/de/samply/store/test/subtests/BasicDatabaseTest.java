/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.test.subtests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import de.samply.store.AccessController;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.history.Transaction;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import de.samply.store.security.PasswordHash;
import de.samply.store.test.TestOntology;

public class BasicDatabaseTest extends AbstractDatabaseTest {

    @Test
    public void logout() throws DatabaseException {
        model.logout();
        assertTrue(model.getUserId() == 0);
    }

    @Test
    public void get() throws DatabaseException {
        Resource location = getLocation();
        assertNotNull(location);
        assertTrue(location.getId() == 1);
    }

    @Test
    public void getCreateAndModify() throws DatabaseException {
        Resource location = getLocation();
        assertNotNull(location);
        assertTrue(location.getId() == 1);

        Resource patient = model.createResource(TestOntology.Type.Patient);
        patient.setProperty("name", "No name available");
        saveResources(patient);

        assertTrue(patient.getId() != 0);

        Resource p = model.getResource(TestOntology.Type.Patient, patient.getId());
        assertTrue(p.getProperty("name").getValue().equals(patient.getProperty("name").getValue()));
        assertTrue(p.getId() == patient.getId());

        p.setProperty("name", "Stefan");
        saveResources(p);

        p = model.getResource(TestOntology.Type.Patient, patient.getId());
        assertTrue(p.getProperty("name").getValue().equals("Stefan"));

        List<Transaction> transactions = model.getTransactions(p);
        assertTrue(transactions.size() == 2);

        p = model.getResource(p, transactions.get(1));
        assertTrue(p.getProperty("name").getValue().equals(patient.getProperty("name").getValue()));

//		model.wipeoutPatient(patient);
    }

    @Test
    public void jsonResource() throws DatabaseException {
        Resource location = getLocation();
        assertNotNull(location);
        assertTrue(location.getId() == 1);

        Resource patient = model.createResource(TestOntology.Type.Patient);
        JSONResource diagnosis = new JSONResource();
        JSONResource cancer = new JSONResource();

        diagnosis.addProperty("blood_pressure", 40.0f);
        diagnosis.addProperty("blood_sugar", 102.3f);
        cancer.setProperty("cancer_location", "Brain");
        cancer.setProperty("cancer_type", "A42");
        cancer.setProperty("cancer_severity", 4);
        diagnosis.setProperty("cancer", cancer);
        diagnosis.setProperty("patient", patient.getIdentifier());

        JSONResource number = new JSONResource();
        number.setProperty("value", 56);
        diagnosis.addProperty("numbers", number);

        number = new JSONResource();
        number.setProperty("value", 40);
        diagnosis.addProperty("numbers", number);

//		patient.setProperty(Ontology.Patient.Group, group);
        patient.setProperty("diagnosis", diagnosis);

        saveResources(patient);

        patient = model.reloadResource(patient);
        assertTrue(patient.getProperty("diagnosis") instanceof JSONResource);

        diagnosis = (JSONResource) patient.getProperty("diagnosis");
        assertTrue(diagnosis.getProperty("cancer") instanceof JSONResource);

        cancer = (JSONResource) diagnosis.getProperty("cancer");

        assertTrue(diagnosis.getProperty("blood_sugar").asFloat() == 102.3f);
        assertTrue(diagnosis.getProperty("blood_pressure").asFloat() == 40.0f);
        assertTrue(cancer.getProperty("cancer_location").getValue().equals("Brain"));
        assertTrue(cancer.getProperty("cancer_type").getValue().equals("A42"));
        assertTrue(cancer.getProperty("cancer_severity").asInteger() == 4);

        assertTrue(diagnosis.getProperties("numbers").size() == 2);
    }

    @Test
    public void criteria() throws DatabaseException {
        Resource location = getLocation();
        assertNotNull(location);

        int r = (new Random()).nextInt();
        float f = (new Random()).nextFloat();
        String hash = PasswordHash.hashPassword("hahaha", PasswordHash.generateSalt());

        Resource patient = model.createResource(TestOntology.Type.Patient);
        patient.setProperty("hash", hash);
        patient.setProperty("age", r);
        patient.setProperty("float", f);

        saveResources(patient);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.addEqual("hash", hash);
        query.addEqual("age", r);
        /**
         * This Test does not work with Float, and I am not sure why.
         */
//		c.addCriteria("float", f);

        ArrayList<Resource> results = model.getResources(query);
        assertTrue(results.size() == 1);
    }

    @Test
    public void resourceIdentifier() throws DatabaseException {
        Resource group = model.getResourceByIdentifier(
                new ResourceIdentifierLiteral(TestOntology.Type.Location, 1).get());
        assertTrue(group.getType().equals(TestOntology.Type.Location));
        assertTrue(group.getId() == 1);
    }

    @Test
    public void regexTest() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);
        patient.setProperty("name", "paula...haha");

        saveResources(patient);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.SimilarTo(TestOntology.Type.Patient, "name", "%paula%"));

        ArrayList<Resource> patients = model.getResources(query);
        assertTrue(patients.size() >= 1);
    }


    @Test
    public void deleteTest() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);
        patient.setProperty("name", "Paulinator_" + (new Random()).nextInt());

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.Equal(TestOntology.Type.Patient, "name", patient.getProperty("name")));

        saveResources(patient);

        assertTrue(model.getResources(query).size() == 1);

        model.reloadResource(patient);
        model.setAccessController(new TestController());
        model.deleteResource(patient);

        assertTrue(model.getResources(query).size() == 0);
    }

    public class TestController extends AccessController<Void> {
        @Override
        public boolean canDeleteResource(Resource resource) {
            return true;
        }
    }

}
