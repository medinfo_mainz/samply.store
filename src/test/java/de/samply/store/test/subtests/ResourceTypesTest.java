/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;

import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.query.ResourceQuery;
import de.samply.store.security.PasswordHash;
import de.samply.store.test.TestOntology;

public class ResourceTypesTest extends AbstractDatabaseTest {

    @Test
    public void testPatientCases() throws DatabaseException {
        Resource group = getLocation();

        Resource patient = model.createResource(TestOntology.Type.Patient);
        Resource CASE = model.createResource(TestOntology.Type.Case);

        CASE.setProperty(TestOntology.Case.Location, group);
        CASE.setProperty(TestOntology.Case.Patient, patient);
        CASE.setProperty("blood_pressure", 56.3f);

        saveResources(patient, CASE);

        CASE = model.reloadResource(CASE);

        assertTrue(CASE.getProperty(TestOntology.Case.Patient) instanceof ResourceIdentifierLiteral);
        ResourceIdentifierLiteral rlit = (ResourceIdentifierLiteral) CASE.getProperty(TestOntology.Case.Patient);

        assertTrue(rlit.getId() == patient.getId());
        assertTrue(rlit.getType().equals(TestOntology.Type.Patient));

        patient = model.reloadResource(patient);

        assertTrue(patient.getProperties(TestOntology.Patient.ReadOnly.Cases).size() == 1);
        assertTrue(patient.getProperties(TestOntology.Patient.ReadOnly.Cases).get(0) instanceof ResourceIdentifierLiteral);

        rlit = (ResourceIdentifierLiteral) patient.getProperties(TestOntology.Patient.ReadOnly.Cases).get(0);
        assertTrue(rlit.getId() == CASE.getId());
        assertTrue(rlit.getType().equals(TestOntology.Type.Case));
    }

    @Test
    public void testContainerTypes() throws DatabaseException {
        Resource type = model.createResource(TestOntology.Type.ContainerType);

        type.setProperty(TestOntology.ContainerType.Size, 4);
        type.setProperty(TestOntology.ContainerType.Label, PasswordHash.generateSalt());

        saveResources(type);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.ContainerType);
        query.addEqual(TestOntology.ContainerType.Size, 4);
        query.addEqual(TestOntology.ContainerType.Label,
                type.getProperty(TestOntology.ContainerType.Label).getValue());

        ArrayList<Resource> types = model.getResources(query);

        assertTrue(types.size() == 1);
    }

    @Test
    public void testQueryAndRequests() throws DatabaseException {
        Resource query = model.createResource(TestOntology.Type.Query);
        query.setProperty("label", "another label");
        query.setProperty("description", "another description");
        query.setProperty(TestOntology.Query.Location, getLocation());

        Resource request = model.createResource(TestOntology.Type.Request);
        request.setProperty(TestOntology.Request.Query, query);

        Resource request2 = model.createResource(TestOntology.Type.Request);
        request2.setProperty(TestOntology.Request.Query, query);

        saveResources(query, request, request2);

        query = model.reloadResource(query);

        assertTrue(query.getProperties(TestOntology.Query.ReadOnly.Requests).size() == 2);

        HashSet<Integer> set = new HashSet<>();
        for(Value v : query.getProperties(TestOntology.Query.ReadOnly.Requests)) {
            ResourceIdentifierLiteral r = (ResourceIdentifierLiteral) v ;
            set.add(r.getId());
        }

        assertTrue(set.contains(request.getId()));
        assertTrue(set.contains(request2.getId()));
    }

    @Test
    public void testPseudonyms() {

    }


}
