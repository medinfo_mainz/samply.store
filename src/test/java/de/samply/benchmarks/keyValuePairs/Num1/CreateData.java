/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.benchmarks.keyValuePairs.Num1;

import java.io.File;
import java.util.Random;

import org.apache.log4j.xml.DOMConfigurator;

import de.samply.store.AccessController;
import de.samply.store.DatabaseModel;
import de.samply.store.Ontology;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;

public class CreateData {

    public static void main(String[] args) throws DatabaseException {
        File file = new File("conf/log4j.xml");
        if(file.exists()) {
            DOMConfigurator.configure("conf/log4j.xml");
        }

        Random rand = new Random(123456);

        DatabaseModel<Void, Void> model = new PSQLModel<>(new AccessController<Void, Void>());

        model.login("admin", "ever");

        int maxKeyValuePairs = 10;
        int maxForms = 10;
        int maxCases = 5;
        int maxPatients = 10000;

        model.beginTransaction();

        Resource location = model.createResource(Ontology.Type.Location);
        location.setProperty(Ontology.Location.Name, "Standort Mainz");

        model.saveOrUpdateResource(location);

        for(int pid = 0; pid < maxPatients; pid++) {
            Resource patient = model.createResource(Ontology.Type.Patient);
            patient.setProperty(Ontology.Patient.Locations, location);
            model.saveOrUpdateResource(patient);

            for(int cid = 0; cid < maxCases; cid++) {
                Resource Case = model.createResource(Ontology.Type.Case);
                Case.setProperty(Ontology.Case.Patient, patient);

                model.saveOrUpdateResource(Case);

                Resource visit = model.createResource(Ontology.Type.Visit);
                visit.setProperty(Ontology.Visit.Case, Case);
                visit.setProperty(Ontology.Visit.Name, "Just another visit " + pid + "_" + cid);

                model.saveOrUpdateResource(visit);

                for(int fid = 0; fid < maxForms; fid++) {
                    Resource form = model.createResource(Ontology.Type.Form);
                    form.setProperty(Ontology.Form.Visit, visit);
                    form.setProperty(Ontology.Form.Name, "Form " + pid + "_" + cid + "_" + fid);
                    form.setProperty(Ontology.Form.State, "open");
                    form.setProperty(Ontology.Form.Version, 1);

                    model.saveOrUpdateResource(form);

                    for(int kid = 0; kid < maxKeyValuePairs; kid++) {
                        Resource pair = model.createResource(Ontology.Type.KeyValuePair);
                        pair.setProperty(Ontology.KeyValuePair.Form, form);
                        pair.setProperty(Ontology.KeyValuePair.Key, "key_" + kid);
                        pair.setProperty(Ontology.KeyValuePair.Value, "value_" + rand.nextInt(192839));

                        model.saveOrUpdateResource(pair);
                    }
                }
            }

            if(pid % 100 == 0) {
                System.out.println("Patient pid: " + pid);
            }
        }

        model.commit();

        model.close();
    }

}
