/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.benchmarks.keyValuePairs.Num1;

import java.io.File;

import org.apache.log4j.xml.DOMConfigurator;

import de.samply.benchmarks.AbstractBenchmark;
import de.samply.store.Ontology;
import de.samply.store.criteria.ComplexCriteria;
import de.samply.store.criteria.Criteria;
import de.samply.store.exceptions.DatabaseException;

public class Benchmark extends AbstractBenchmark {

    public static void main(String[] args) throws DatabaseException {
        File file = new File("conf/log4j.xml");
        if(file.exists()) {
            DOMConfigurator.configure("conf/log4j.xml");
        }

        Benchmark b = new Benchmark();
        b.run();
    }

    public Benchmark() throws DatabaseException {
    }

    public void run() throws DatabaseException {
        runBenchmark1();

        runBenchmark2();

        runBenchmark3();

        model.close();
    }

    private void runBenchmark1() throws DatabaseException {
        ComplexCriteria criteria = new ComplexCriteria(Ontology.Type.Patient);
        criteria.add(Criteria.Equal(Ontology.Type.KeyValuePair, Ontology.KeyValuePair.Key, "key_1"));
        criteria.add(Criteria.Equal(Ontology.Type.KeyValuePair, Ontology.KeyValuePair.Value, "value_123234"));

        benchmarkCriteria(criteria, 1);
    }

    private void runBenchmark2() throws DatabaseException {
        ComplexCriteria criteria = new ComplexCriteria(Ontology.Type.KeyValuePair);
        criteria.addEqual(Ontology.KeyValuePair.Key, "key_1");
        criteria.addEqual(Ontology.KeyValuePair.Value, "value_123234");

        benchmarkCriteria(criteria, 2);
    }

    private void runBenchmark3() throws DatabaseException {
        ComplexCriteria criteria = new ComplexCriteria(Ontology.Type.Patient);
        criteria.add(Criteria.Or(
                Criteria.And(
                        Criteria.Equal(Ontology.Type.KeyValuePair, Ontology.KeyValuePair.Key, "key_1"),
                        Criteria.Equal(Ontology.Type.KeyValuePair, Ontology.KeyValuePair.Value, "value_123234")),
                Criteria.And(
                        Criteria.Equal(Ontology.Type.KeyValuePair, Ontology.KeyValuePair.Key, "key_4"),
                        Criteria.Equal(Ontology.Type.KeyValuePair, Ontology.KeyValuePair.Value, "value_23456"))));

        benchmarkCriteria(criteria, 3);
    }
}
