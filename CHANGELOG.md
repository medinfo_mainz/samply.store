# Changelog Samply Store

## Version 1.1.2

- the methods `saveResource` and `updateResource` now call the AccessController
  to remove unnecessary properties
